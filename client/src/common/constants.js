
export const BASE_URL = 'http://localhost:5000';

export const FILE_URL = 'http://localhost:5000/app/';

export const SUGGESTED_PLAYLISTS_PAGE_SIZE = 5;

export const SUGGESTED_PLAYLISTS_PAGE_SIZE_LAPTOP = 4;
