import "./Sidebar.css";
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";

const Sidebar = ({ modalShow, setModalShow, popUpMessage }) => {

	return (
		<div>

		<SlidingPane
		  className="some-custom-class"
		  overlayClassName="some-custom-overlay-class"
		  isOpen={modalShow}
		  onRequestClose={() => {
			setModalShow(false);
		  }}>
		  <p className="errorString">{popUpMessage}</p>
		  <img className="slidingImage" src="https://scontent.fsof9-1.fna.fbcdn.net/v/t1.15752-9/191877148_225526365715866_5266442975512321066_n.png?_nc_cat=107&ccb=1-3&_nc_sid=ae9488&_nc_ohc=e4AjU_p8MvQAX-7l6Xu&_nc_ht=scontent.fsof9-1.fna&oh=dffe2d09f4ca90e47088ca7982695260&oe=60D35DCA" />
		</SlidingPane>

	  </div>

	)

}

export default Sidebar;