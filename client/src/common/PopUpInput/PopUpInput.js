import "./PopUpInput.css";
import SlidingPane from "react-sliding-pane";
import "react-sliding-pane/dist/react-sliding-pane.css";
import { Button, Modal } from 'react-bootstrap';
import { useRef, useState } from 'react';

const PopUpInput = ({ type, title, message, btnText, action, closeModal }) => {
    const [banDays, setBanDays] = useState(3);
    const handleAction = () => {
        if (type === 'ban') action(type, banDays);
        if (type === 'liftBan') action(type);
        if (type === 'deleteUser') action(type);

    };
    const handleCloseModal = () => closeModal();

    return (

        <Modal animation={false} show={true} onHide={handleCloseModal}>
            <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
            </Modal.Header>
            {type === 'ban' ? <Modal.Body>{message}<input onChange={(event) => setBanDays(event.target.value)}/></Modal.Body> : null}
            {type === 'liftBan' ? <Modal.Body>{message}</Modal.Body> : null}
            {type === 'deleteUser' ? <Modal.Body>{message}</Modal.Body> : null}

            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseModal}>Close</Button>
                <Button variant="primary" onClick={handleAction}>{btnText}</Button>
            </Modal.Footer>
        </Modal>

    )

}

export default PopUpInput;