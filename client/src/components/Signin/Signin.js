import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import { useState, useEffect, useContext } from 'react';
import './Signin.css';
import AuthContext, { extractUser } from '../../providers/AuthContext';
import { BASE_URL } from '../../common/constants';
import { Link } from 'react-router-dom';
import Spinner from 'react-bootstrap/Spinner';
import { Container } from 'react-bootstrap';
import Loading from '../Loading/Loading'
import Sidebar from '../../common/Sidebar/Sidebar';


const Signin = ({ history, location, match }) => {
    const login = location.pathname === '/login';

    const [loading, setLoading] = useState(false);
    const [message, setMessage] = useState('');
    const [popUpMessage, setPopUpMessage] = useState('');
    const [modalShow, setModalShow] = useState(false);
    const [isFormValid, setIsFormValid] = useState(false);
    const [form, setForm] = useState({
        username: {
            placeholder: "username",
            value: '',
            className: "input-form",
            type: 'text',
            validate: (value) => value.length > 4 && value.length < 21 && /^[A-Za-z0-9]+$/.test(value),
            valid: false,
            errMessage: 'Username length must be between 4 and 20 without special characters.',
            errMessagePopUp: 'Username Invalid'
        },
        password: {
            placeholder: 'password',
            value: '',
            className: "input-form",
            type: "password",
            validate: (value) => /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{7,15}$/.test(value),
            valid: false,
            errMessage: 'Password must be between 7 to 15 characters with at least one numeric and a special characters.',
            errMessagePopUp: 'Password Invalid'
        },
        confirmPassword: {
            placeholder: 'confirm password',
            value: '',
            className: "input-form",
            type: "password",
            validate: (value) => value === form.password.value,
            valid: true,
            errMessage: 'Password inputs does not match.',
            errMessagePopUp: 'Same thing, type twice.'
        }
    });
    const [error, setError] = useState(null);

    const { isLoggedIn, setLoginState } = useContext(AuthContext);

    useEffect(() => {
        if (isLoggedIn) {
            history.push('/');
        }
    }, [history, isLoggedIn]);

    const handleSubmit = event => {
        event.preventDefault();

        Object.values(form).forEach(el => {
            if (el.valid === false) {
                setMessage(el.errMessage);
                setPopUpMessage(el.errMessagePopUp);
            }
        })

        if (login) {
            if (form.username.value === "" || form.password.value === "") {
                setPopUpMessage("All inputs you must fill")
            }
        } else {
            Object.values(form).forEach(el => {
                if (el.value === "") {
                    setPopUpMessage("All inputs you must fill")
                }
            })
        }

        if (!isFormValid) {
            setModalShow(true);
            return;
        }

        const data = Object.keys(form).reduce((acc, key) => {
            return {
                ...acc,
                [key]: form[key].value
            }
        }, {});

        setLoading(true);

        if (login) {
            fetch(`${BASE_URL}/auth/signin`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            })
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error)
                    }
                    const userStatus = extractUser(res.token);

                    if (userStatus.isBanned) {
                        setPopUpMessage(`${userStatus.isBanned.split(" ")[0]} banned until`);
                        setModalShow(true);
                        return;
                    }

                    if (userStatus.isDeleted) {
                        setPopUpMessage("Away you go deleted user");
                        setModalShow(true);
                        return;
                    }

                    localStorage.setItem('token', res.token);

                    setLoginState({
                        isLoggedIn: !!extractUser(res.token),
                        user: extractUser(res.token),
                    });

                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));
        } else {
            fetch(`${BASE_URL}/users`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8'
                }
            })
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error);
                    }
                    history.push('/login')
                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));
        }

    }

    const handleInputChange = (event) => {
        const { name, value } = event.target;

        form[name].value = value;
        form[name].valid = form[name].validate(value)

        const updatedForm = { ...form }
        setForm(updatedForm);

        if (login) {
            const formValid = [updatedForm.username, updatedForm.password].every(elem => elem.valid)
            setIsFormValid(formValid);
        } else {
            form.confirmPassword.valid = form.confirmPassword.validate(form.confirmPassword.value)
            const formValid = Object.values(updatedForm).every(elem => elem.valid);
            setIsFormValid(formValid);
        }

    };

    const formElements = Object.keys(form)
        .map(name => {
            return {
                id: name,
                config: form[name]
            };
        })
        .map(({ id, config }) => {
            if (login === false || id !== 'confirmPassword') return (
                <Form.Group key={id}>
                    <Form.Control className="input-form" type={config.type} name={id} placeholder={config.placeholder} value={config.value} onChange={handleInputChange} />
                    {!config.valid && message !== '' ? <span className="loginError">{config.errMessage}</span> : null}
                </Form.Group>
            )
        })

    if (error) {
        setError(null);
        setPopUpMessage(error)
        setModalShow(true);
    }

    if (loading) {
        return (
            <div>
                <Container>
                    <Loading>
                        <Spinner animation="border" />
                    </Loading>
                </Container>
            </div>
        )
    }
    return (
        <>
            {modalShow ? <Sidebar modalShow={modalShow} setModalShow={() => setModalShow(false)} popUpMessage={popUpMessage} /> : null}
            <Form className="login-form" onSubmit={handleSubmit}>
                {formElements}
                <Button variant="light" type="submit">
                    {login ? 'Login' : 'Register'}
                </Button>
                <Link to={login ? '/register' : '/login'} className="link">
                    {login ? 'Create an account' : `Already have an account?`}
                </Link>
            </Form>
        </>
    )
}

export default Signin;