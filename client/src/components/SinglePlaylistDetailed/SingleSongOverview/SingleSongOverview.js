import { Container, Row, Col, Image } from "react-bootstrap";
import "./SingleSongOverview.css"

const SingleSongOverview = ({ title, author, setPreviewSongId, id }) => {

    return (
        <Container className="playlistSongs">
            <div className="singleSong">
                <div sm={10} className="songInfo">
                    <div className="songTitle">{title} </div>
                    <div className="songAuthor">{author} </div>
                </div>
                <div sm={2} className="songButtons">
                    <Image className='previewSongButton' src='/playButton.png' onClick={() => setPreviewSongId(id)}/>
                </div>
            </div>
        </Container>
    )

}

export default SingleSongOverview;