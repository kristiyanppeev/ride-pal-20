import Modal from 'react-bootstrap/Modal';
import { Button } from "react-bootstrap";

const DeleteConfirmation = ({ showConfirmationModal, setShowConfirmationModal, id, deletePlaylist }) => {

    return (
        <>
            <Modal
                show={showConfirmationModal}
                onHide={() => setShowConfirmationModal(false)}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Are you sure you want to delete this playlist?</Modal.Title>
                </Modal.Header>
                {/* <Modal.Body>
                    Are you sure you want to delete this playlist?
          </Modal.Body> */}
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setShowConfirmationModal(false)}>
                        Close
            </Button>
                    <Button variant="primary" onClick={() => deletePlaylist(id)}>Confirm</Button>
                </Modal.Footer>
            </Modal>
        </>
    )

}

export default DeleteConfirmation;