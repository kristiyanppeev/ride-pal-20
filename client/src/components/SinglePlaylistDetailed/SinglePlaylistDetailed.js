import { useContext, useEffect, useState } from "react";
import Spinner from 'react-bootstrap/Spinner';
import { Container, Image, Row, Col } from "react-bootstrap";
import AppError from "../AppError/AppError";
import { BASE_URL } from "../../common/constants";
import SingleSongOverview from "./SingleSongOverview/SingleSongOverview";
import './SinglePlaylistDetailed.css';
import AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';
import AuthContext, { extractUser, getToken } from "../../providers/AuthContext";
import Chip from '@material-ui/core/Chip';
import { v4 as uuidv4 } from 'uuid';
import Sidebar from "../../common/Sidebar/Sidebar";
import DeleteConfirmation from "./DeleteConfirmationModal/DeleteConfirmation";

const SinglePlaylistDetailed = ({ match, history }) => {

    const playlistId = match.params.id;

    const [showConfirmationModal, setShowConfirmationModal] = useState(false);
    const [popUpMessage, setPopUpMessage] = useState('');
    const [modalShow, setModalShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [playlistSongs, setPlaylistSongs] = useState(null);
    const [songElements, setSongElements] = useState([]);
    const [previewSongId, setPreviewSongId] = useState(false);
    const [playlistInfo, setPlaylistInfo] = useState(null);
    const [authorName, setAuthorName] = useState(null);
    const [playlistTagsChips, setPlaylistTagsChips] = useState([]);
    const [editMode, setEditMode] = useState(false);
    const [playlistTitle, setPlaylistTitle] = useState(null);
    const [playlistTags, setPlaylistTags] = useState([]);
    const [newTag, setNewTag] = useState("");
    const { isLoggedIn, user } = useContext(AuthContext);

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/users/playlists/get/${playlistId}`)
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw new Error(res.error)
                }
                setPlaylistSongs(res.playList[0].combinedGenresSongs);
                delete res.playList[0].combinedGenresSongs;
                setPlaylistInfo(res.playList[0]);
                setPlaylistTitle(res.playList[0].playlist_title);
                setPlaylistTags(res.playList[0].tags)
            })
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));
    }, []);

    useEffect(() => {
        if (playlistSongs) {
            setLoading(true);
            playlistSongs.forEach(el => {
                fetch(`${BASE_URL}/users/artists/${el.artists_artist_id}`)
                    .then(res => res.json())
                    .then(res => {
                        if (res.error) {
                            throw new Error(res.error)
                        }
                        el.authorName = res.name;

                        const songs = playlistSongs.map(el => <SingleSongOverview
                            key={el.track_id}
                            title={el.title}
                            author={el.authorName}
                            setPreviewSongId={setPreviewSongId}
                            id={el.track_id}
                        />)
                        setSongElements(songs);

                    })
                    .catch(err => setError(err.message))
                    .finally(() => {
                        setLoading(false);
                    });
            })
        }
    }, [playlistSongs]);

    useEffect(() => {
        if (playlistInfo) {
            setLoading(true);
            fetch(`${BASE_URL}/users/${playlistInfo.users_id}`)
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error)
                    };
                    setAuthorName(res.username);
                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));
        }
    }, [playlistInfo]);

    useEffect(() => {
        if (playlistInfo) {
            const playlistTagsTemp = playlistTags.map((el, index) => {

                const deletingFunction = (ind) => {
                    playlistTags.splice(ind, 1);
                    setPlaylistTags([...playlistTags]);
                }

                return (
                    <Chip
                        key={uuidv4()}
                        className="chipTagsSinglePlaylist"
                        label={el}
                        onDelete={editMode ? () => deletingFunction(index) : null}
                        variant="outlined"
                    />)
            })
            setPlaylistTagsChips(playlistTagsTemp);
        }
    }, [playlistTags, editMode])

    const deletePlaylist = (id) => {

        setLoading(true);
        if (user && user.role === "user") {
            fetch(`${BASE_URL}/users/playlists/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${getToken()}`,
                },
            })
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error)
                    };
                    history.goBack();
                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));
        } else if (user && user.role === "admin") {
            fetch(`${BASE_URL}/admins/playlists/${id}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${getToken()}`,
                },
            })
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error)
                    };
                    history.goBack();
                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));
        }
        setLoading(false);
    }

    const handleEditTitle = (event) => {
        setPlaylistTitle(event.target.value)
    }

    const handleNewTagChange = (event) => {
        setNewTag(event.target.value)
    }

    const handleKeyPress = (event) => {
        const x = event.which || event.keyCode;

        if (x === 13) {
            if (newTag.length < 5 || newTag.length > 14) {
                setPopUpMessage("Between 4 and 15 tag length must be.")
                setModalShow(true);
                return;
            }

            setPlaylistTags([...playlistTags, newTag])
            setNewTag('');
        }
    }

    const editPlaylist = () => {
        if (playlistTitle.length < 6 || playlistTitle.length > 49) {
            setPopUpMessage("Between 5 and 50 title length must be.")
            setModalShow(true);
            return;
        }
        setLoading(true);
        const reqBody = {
            playListTitle: playlistTitle,
            tags: playlistTags
        };
        if (user && user.role === "user") {
            fetch(`${BASE_URL}/users/playlists/${playlistId}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${getToken()}`,
                },
                body: JSON.stringify(reqBody)
            })
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error)
                    };
                    setEditMode(false);
                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));
        } else if (user && user.role === "admin") {
            fetch(`${BASE_URL}/admins/playlists/${playlistId}`, {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${getToken()}`,
                },
                body: JSON.stringify(reqBody)
            })
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error)
                    };
                    setEditMode(false);
                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));
        }

        setLoading(false);
    }

    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <Container>
                <Spinner animation="border" />
            </Container>
        )
    }
    // console.log(playlistInfo)
    return (
        <div>
            <Container className="singlePlaylistContainer">
                {modalShow ? <Sidebar modalShow={modalShow} setModalShow={() => setModalShow(false)} popUpMessage={popUpMessage} /> : null}
                {showConfirmationModal ?
                    <DeleteConfirmation
                        showConfirmationModal={showConfirmationModal}
                        setShowConfirmationModal={setShowConfirmationModal}
                        id={playlistId}
                        deletePlaylist={deletePlaylist}
                    />
                    : null}
                <Row className='singlePlaylistTitle'>
                    {playlistInfo &&
                        (!editMode ?
                            playlistTitle :
                            <input type="text" value={playlistTitle} onChange={e => handleEditTitle(e)} className="editTitleInput" name="lname" />)}</Row>
                <Row className='singlePlaylistAuthor'>{authorName}</Row>
                <Row>
                    <Col className="playlistBackButton">
                        <Image className='playlistMainButtons' onClick={() => history.goBack()} src='/backspace-xxl.png' />
                    </Col>
                    {playlistInfo && user && (playlistInfo.users_id === user.sub || user.role === "admin" ? <Col className='singlePlaylistHeader'>
                        {!editMode ?
                            <Image className='playlistMainButtons' src='/edit-xxl.png' onClick={() => setEditMode(true)} /> :
                            <Image className='playlistMainButtons' src='/checkmark-xxl.png' onClick={editPlaylist} />}
                        <Image className='playlistMainButtons' src='/delete-2-xxl.png' onClick={() => setShowConfirmationModal(true)} />
                    </Col> : null)}
                </Row>
                <Row className='singlePlaylistInfo'>
                    <Col>{playlistInfo && `${Math.floor(playlistInfo.duration / 3600)}:${Math.floor((playlistInfo.duration % 3600) / 60)}min`}</Col>
                    <Col>{playlistInfo && `${(playlistInfo.rank / 100000).toFixed(1)}/10`}</Col>
                </Row>
                <Row className='singlePlaylistInfo'>
                    {playlistTagsChips}
                </Row>
                <Row className='singlePlaylistInfo'>
                    {editMode && playlistTags.length < 5 ?
                        <input type="text" value={newTag} onKeyPress={handleKeyPress} onChange={handleNewTagChange} className="editTitleInput editGenresInput" placeholder="Add tag..." /> :
                        null}
                </Row>
                <Row>
                    {songElements}
                </Row>
            </Container>
            {previewSongId && <AudioPlayer
                className="audioPlayer"
                autoPlay
                src={previewSongId ? playlistSongs.filter(el => el.track_id === previewSongId)[0].preview : null}
                header={<div className="playerHeader">{previewSongId ? playlistSongs.filter(el => el.track_id === previewSongId)[0].title : null}
                    <Image className='closePlayerButton' src='/close-button.png' onClick={() => setPreviewSongId(false)} /></div>}
            />}
        </div>
    )

}

export default SinglePlaylistDetailed;