import { useEffect, useState } from 'react';
import { BASE_URL } from '../../common/constants.js'
import AppError from '../AppError/AppError.js';
import Spinner from 'react-bootstrap/Spinner';
import { Container } from "react-bootstrap";
import Loading from '../Loading/Loading.js';
import './Homepage.css';

const Homepage = ({ history }) => {

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  let index = 0;
  const [playlists, setPlaylists] = useState([]);

  let transform = [];
  let zIndex = [];

  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/users/playlists/getall?page=0&pageSize=10`)
      .then(res => res.json())
      .then(res => {
        if (res.error) {
          throw new Error(res.error)
        }

        res.playLists = res.playLists.filter(el => el.combinedGenresSongs.length !== 0);

        res.playLists.forEach((el) => {

          fetch(`${BASE_URL}/users/artists/${el.combinedGenresSongs[0].artists_artist_id}`)
            .then(res => res.json())
            .then(res => {
              if (res.error) {
                throw new Error(res.error)
              }
              el.cover = res.picture;

            })
            .catch(err => setError(err.message))

        })

        res.playLists.forEach(playlist => {

          Object.entries(playlist.genres).forEach(el => {
            if (el[1] > 0) {
              fetch(`${BASE_URL}/users/genres/${el[0]}`)
                .then(response => response.json())
                .then(response => {
                  if (response.error) {
                    throw new Error(response.error)
                  };

                  if(!playlist.genresNames) {
                    playlist.genresNames = response.name;
                  } else {
                    playlist.genresNames = `${playlist.genresNames}, ${response.name}`
                  }
                })
                .catch(err => setError(err.message))
                .finally(() => setPlaylists([...res.playLists]));
            }
          })

        })

      })
      .catch(err => setError(err.message))
      .finally(() => setLoading(false));
  }, [])

  const elements = playlists.map((el, i) => {
    transform.push(`translate3d(${i * 150}px, 0px, -${i * 1000}px)`);
    zIndex.push(`-${i}`);

    if (i === playlists.length - 1) {
      showSlide(index);
    }
    const hours = Math.floor(el.duration / 3600);
    const minutes = Math.floor((el.duration % 3600) / 60);
    return (
      <div className="slide" key={el.playlist_id} onClick={() => history.push(`/playlist/${el.playlist_id}`)} style={{ transform: transform[i], zIndex: zIndex[i] }}>
        <div className="item">
          <img className='img' src={el.cover} alt={el.playlist_title} />
          <div className="text" style={{ opacity: index = i ? 0 : 1 }}>
            <h1 className="title">{el.playlist_title}</h1>
            <h2 className="year">{el.genresNames}</h2>
            <h2 className="year">{hours}:{minutes}min</h2>
          </div>
        </div>
      </div>
    )
  })

  function onChange(toIndex) {
    if (toIndex === 1) {
      transform.unshift(transform[transform.length - 1]);
      transform.pop();
      zIndex.unshift(zIndex[zIndex.length - 1]);
      zIndex.pop();
    } else {
      transform.push(transform[0]);
      transform.shift();
      zIndex.push(zIndex[0]);
      zIndex.shift();
    }
    showSlide((index += toIndex));
  }
  function showSlide(toIndex) {
    let i;
    const slides = document.getElementsByClassName("slide");
    const text = document.getElementsByClassName("text");

    if (toIndex >= playlists.length) {
      index = 0;
    }
    if (toIndex < 0) {
      index = playlists.length - 1;
    }
    for (i = 0; i < slides.length; i++) {
      slides[i].style.transform = transform[i];
      slides[i].style.zIndex = zIndex[i];
      text[i].style.opacity = index === i ? 1 : 0;
    }
  }

  if (error) {
    return (
      <AppError message={error} />
    )
  }

  if (loading) {
    return (
      <div>
        <Container>
          <Loading>
            <Spinner animation="border" />
          </Loading>
        </Container>
      </div>
    )
  }

  return (
    <div id="containers" className="containers">
      {/* <svg id="starField" />       */}
      <p className="homepageTitle">Top ranked playlists</p>
      <div className="buttonContainers">
        <button id="backButton" onClick={() => onChange(-1)} className="buttonbacknext" aria-label="back_button" />
        <button id="nextButton" onClick={() => onChange(+1)} className="buttonbacknext" aria-label="next_button" />
      </div>
      <div id="slideContainer" className="slideContainer" >
        {elements}
      </div>
    </div>
  )
}

export default Homepage;
