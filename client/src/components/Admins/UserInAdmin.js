import { useEffect, useState } from 'react';
import { Container, Col, Row, Image, Button, Spinner, Modal } from "react-bootstrap";
import { useHistory } from "react-router";
import { BASE_URL } from '../../common/constants';
import PopUpInput from '../../common/PopUpInput/PopUpInput';
import Sidebar from '../../common/Sidebar/Sidebar';
import { getToken } from '../../providers/AuthContext';
import AppError from '../AppError/AppError';
import Loading from '../Loading/Loading';
import './UserInAdmin.css'


const UserInAdmin = ({ id, username, is_banned, is_deleted, profile_pic, role, refreshUser }) => {

    const [popUpMessage, setPopUpMessage] = useState('');
    const [modalShow, setModalShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const [banUser, setBanUser] = useState(null);
    const [liftBanUser, setLiftBanUser] = useState(null);
    const [deleteUser, setDeleteUser] = useState(null);

    const [banDays, setBanDays] = useState(null);
    const [askModal, setAskModal] = useState(null);
    const handleRefreshUser = () => refreshUser();

    const [banSeconds, setBanSeconds] = useState(is_banned);
    const [days, setDays] = useState(null);


    useEffect(() => {
        setBanSeconds(is_banned);
    }, [is_banned]);

    useEffect(() => {
        if (is_banned === 'Not banned' || is_deleted === 'User is deleted') return;

        const timerID = setInterval(() => tick(), 1000);
        return function cleanup() {
            clearInterval(timerID);
        };
    });

    const tick = () => {
        setBanSeconds(+banSeconds - 1);
        setDays(Math.ceil(banSeconds / 60 / 60 / 24));
    }

    useEffect(() => {
        // setLoading(true);
        if (banUser === null) return;

        fetch(`${BASE_URL}/admins/${id}/ban/${banDays}`, {
            mode: 'cors',
            method: 'POST',
            headers: {
                Authorization: `Bearer ${getToken()}`,
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    setPopUpMessage("Faulty our server is. Sorry we are!");
                    setModalShow(true);
                    return;
                }
                if (res.message) {
                    setPopUpMessage(`A user was banned! For ${banDays} ${banDays > 1 ? 'days' : 'day'}!`);
                    setModalShow(true);
                    return;
                }
            }).then(res => {
                handleRefreshUser();
            })
            .catch(err => setError(err.message))
            .finally(() => {
                // setLoading(false)
            })
    }, [banUser])

    useEffect(() => {
        // setLoading(true);
        if (liftBanUser === null) return;
        console.log('lift ban')

        fetch(`${BASE_URL}/admins/liftban/${id}`, {
            mode: 'cors',
            method: 'POST',
            headers: {
                Authorization: `Bearer ${getToken()}`,
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    setPopUpMessage("Faulty our server is. Sorry we are!");
                    setModalShow(true);
                    return;
                }
                if (res.message) {
                    console.log(res.message)
                    setPopUpMessage(`Returned to us, the user is!`);
                    setModalShow(true);
                    return;
                }
            }).then(res => {
                handleRefreshUser();
            })
            .catch(err => setError(err.message))
            .finally(() => {
                // setLoading(false)
            })
    }, [liftBanUser])

    useEffect(() => {
        // setLoading(true);
        if (deleteUser === null) return;
        console.log('delete')

        fetch(`${BASE_URL}/admins/${id}`, {
            mode: 'cors',
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${getToken()}`,
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    console.log(res.error)
                    setPopUpMessage("Faulty our server is. Sorry we are!");
                    setModalShow(true);
                    return;
                }
                if (res.message) {
                    console.log(res.message)
                    setPopUpMessage(`Banished, the user is!`);
                    setModalShow(true);
                    return;
                }
            }).then(res => {
                handleRefreshUser();
            })
            .catch(err => setError(err.message))
            .finally(() => {
                // setLoading(false)
            })
    }, [deleteUser])

    const adminModalInfo = (type, days) => {
        setBanDays(days);
        setBanUser(type === 'ban' ? 'ban' : null);
        setLiftBanUser(type === 'liftBan' ? 'liftBan' : null);
        setDeleteUser(type === 'deleteUser' ? 'deleteUser' : null);
        setAskModal(null);
    }

    const closeModal = () => {
        setAskModal(null);
    }

    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <div>
                <Container>
                    <Loading>
                        <Spinner animation="border" />
                    </Loading>
                </Container>
            </div>
        )
    }

    return (
        <div className="userDetails">
            {modalShow ? <Sidebar modalShow={modalShow} setModalShow={() => setModalShow(false)} popUpMessage={popUpMessage} /> : null}
            <Row className="userDetailsAll">
                <Col sm={3} >
                    <Image className="profileImage" src={profile_pic} roundedCircle />
                </Col>

                <Col className="userDetailsText">
                    <Row>
                        <h1 className="textDetails">{username}</h1>
                    </Row>
                    <Row>
                        <h1 className="textDetails">{role}</h1>
                    </Row>
                    <Row>
                        <h1 className="textDetails">{is_deleted}</h1>
                    </Row>
                    <Row>
                        <h1 className="textDetails">{is_banned === 'Not banned' ? is_banned :
                            `Banned for ${days} ${days > 1 ? 'days' : 'day'}`}</h1>
                    </Row>
                    <Row>
                            {is_banned !== 'Not banned' ? <h1 className="textDetails">{is_banned === 'Not banned' ? is_banned :
                            `Or ${banSeconds} seconds :D`}</h1> : null}
                    </Row>
                </Col>
            </Row>

            <Row className="manageUserBtns">
                <Col className="banBtn" style={{ marginLeft: '-1rem', paddingLeft: '0rem', paddingRight: '0%' }}>
                    <Button variant="light"
                        onClick={() => setAskModal('ban')}
                        disabled={((is_banned === 'Not banned') && (is_deleted === 'User can be deleted')) ? false : true}>Ban
                    </Button>
                    {askModal === 'ban' ? <PopUpInput type={'ban'} title={'Ban the user?'} message={"Careful you should be! A dark path, you may unravel!"} btnText={"Ban"} action={adminModalInfo} closeModal={closeModal} /> : null}
                </Col>
                <Col className="liftBanBtn" style={{ paddingLeft: '0%', paddingRight: '0%' }}>
                    <Button variant="light"
                        onClick={() => setAskModal('liftBan')}
                        disabled={((is_banned === 'Not banned') || (is_deleted !== 'User can be deleted')) ? true : false}>Lift Ban
                    </Button>
                    {askModal === 'liftBan' ? <PopUpInput type={'liftBan'} title={'Lift this curse you want?'} message={"Sure you are, to the right path this user is?"} btnText={"Back to the light!"} action={adminModalInfo} closeModal={closeModal} /> : null}
                </Col>
                <Col className="deleteBtn" style={{ paddingLeft: '0%', paddingRight: '10%' }}>
                    <Button variant="light"
                        onClick={() => setAskModal('deleteUser')}
                        disabled={is_deleted === 'User can be deleted' ? false : true}>Delete
                    </Button>
                    {askModal === 'deleteUser' ? <PopUpInput type={'deleteUser'} title={'Ban the poor soul?'} message={"A terrible fate this is. <(°╭╮°)>"} btnText={"Delete"} action={adminModalInfo} closeModal={closeModal} /> : null}
                </Col>
            </Row>
        </div>
    )
};

export default UserInAdmin;