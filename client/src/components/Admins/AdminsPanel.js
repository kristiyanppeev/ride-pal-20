import { useEffect, useState } from "react";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './AdminsPanel.css'
import { Button, Container } from "react-bootstrap";
import Sidebar from "../../common/Sidebar/Sidebar";
import { BASE_URL, FILE_URL } from "../../common/constants";
import Spinner from 'react-bootstrap/Spinner';
import Loading from "../Loading/Loading";
import AppError from '../AppError/AppError';
import { getToken } from "../../providers/AuthContext";
import UserInAdmin from './UserInAdmin';

const AdminsPanel = () => {

    const [popUpMessage, setPopUpMessage] = useState('');
    const [modalShow, setModalShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const [userName, setUserName] = useState({ userName: '' });
    const [searchUserName, setSearchUserName] = useState(null);
    const [user, setUser] = useState(null);

    useEffect(() => {
        // setLoading(true);
        if (searchUserName === null) return;
        console.log('Changed')
        fetch(`${BASE_URL}/admins/users/${searchUserName.userName}`, {
            headers: {
                Authorization: `Bearer ${getToken()}`,
            }
        })
            .then(res => res.json())
            .then(res => {
                if (userName.userName === '') {
                    setPopUpMessage("Enter a name, you must!")
                    setModalShow(true);
                    return;
                }
                if (res.error) {
                    setPopUpMessage("Faulty our server is. Sorry we are!")
                    setModalShow(true);
                    return;
                }
                if (res.message) {
                    setPopUpMessage("No such user there is!")
                    setModalShow(true);
                    return;
                }
                res.username = `Username: ${res.username}`
                res.role = `Role: ${res.role}`

                let seconds = 0;
                if (res.is_banned !== null) {
                    const currentDate = new Date().getTime();
                    const bannTime = new Date(res.is_banned).getTime();
                    const remainingMiliseconds = bannTime - currentDate;
                    seconds = Math.floor(remainingMiliseconds / 1000);
                }
                console.log(seconds)
                res.is_banned = res.is_banned === null ? 'Not banned' : seconds;
                res.is_deleted = res.is_deleted === 0 ? 'User can be deleted' : 'User is deleted';
                if (res.profile_pic === null) res.profile_pic = 'default.png';
                setUser(res)
            })
            .catch(err => setError(err.message))
            .finally(() => {
                // setLoading(false)
            })
    }, [searchUserName])

    const refreshUser = () => {
        setSearchUserName({ userName: userName.userName });
    }

console.log(user)
    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <div>
                <Container>
                    <Loading>
                        <Spinner animation="border" />
                    </Loading>
                </Container>
            </div>
        )
    }

    return (
        <div className="adminPanel">
            {modalShow ? <Sidebar modalShow={modalShow} setModalShow={() => setModalShow(false)} popUpMessage={popUpMessage} /> : null}
            <Col>
                <h3 className="adminTitle">Admins Panel</h3>
                <h3 className="adminHeader">Find User</h3>
                <Row>
                    <Form.Control type="text" className="usernameInput" onChange={(event) => { setUserName({ userName: event.target.value }) }} placeholder="username" />
                </Row>

                <Row>
                    <Button className="findButton" variant="light" onClick={() => setSearchUserName({ userName: userName.userName })}>Find</Button>
                </Row>

                <Row className="searchResults">
                    {user && <UserInAdmin id={user.id} username={user.username} is_banned={user.is_banned} is_deleted={user.is_deleted} profile_pic={`${FILE_URL}${user.profile_pic}`} role={user.role} refreshUser={refreshUser} />}
                </Row>

            </Col>
        </div>
    )
}

export default AdminsPanel;
