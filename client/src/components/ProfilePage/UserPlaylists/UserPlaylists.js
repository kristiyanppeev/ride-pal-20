import { useEffect, useState } from "react";
import { BASE_URL } from "../../../common/constants";
import AppError from "../../AppError/AppError";
import { Container } from "react-bootstrap";
import Spinner from 'react-bootstrap/Spinner';
import SinglePlaylistOverview from '../../SinglePlaylistOverview/SinglePlaylistOverview';

const UserPlaylists = ({ userId }) => {

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [userPlaylists, setUserPlaylists] = useState([]);
    const [playlistsInfo, setPlaylistsInfo] = useState([]);
    const [allGenres, setAllGenres] = useState(null);

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/users/playlists/author/${userId}`)
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw new Error(res.error)
                }
                setUserPlaylists(res);
            })
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));
    }, []);

    useEffect(() => {
        if (userPlaylists) {
            setLoading(true);
            const userPlaylistsPromises = userPlaylists.map(el => fetch(`${BASE_URL}/users/playlists/get/${el.playlist_id}`).then(res => res.json()))

            Promise.all(userPlaylistsPromises)
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error)
                    }
                    const playlists = res.map(el => el.playList[0]).filter(el => el.combinedGenresSongs.length > 0);
                    
                    const playlistsWithAuthorName = playlists.map(el => fetch(`${BASE_URL}/users/artists/${el.combinedGenresSongs[0].artists_artist_id}`).then(artist => artist.json()))

                    Promise.all(playlistsWithAuthorName)
                        .then(artist => {
                            if (res.error) {
                                throw new Error(res.error)
                            }
                            // console.log(artist)
                            // el.authorName = artist.name;
                            const fullPlaylistInfo = artist.map((el, index) => {
                                return { ...playlists[index], cover: el.picture }
                            })

                            setPlaylistsInfo(fullPlaylistInfo)
                        })
                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));
        }
    }, [userPlaylists])

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/users/allgenres`)
        .then(res => res.json())
        .then(res => {
            if (res.error) {
                throw new Error(res.error)
            };
            setAllGenres(res);
        })
        .catch(err => setError(err.message))
        .finally(() => setLoading(false));
    }, [])

    const userPlaylistsElements = playlistsInfo.map(el => {

        const calculatedRank = `${(el.rank/100000).toFixed(1)}/10`;
        const hours = Math.floor(el.duration / 3600);
        const minutes = Math.floor((el.duration % 3600) / 60);
        const formattedDuration = `${hours}:${minutes} min`;
        const genres = Object.entries(el.genres).filter(el => el[1] !== 0).map(el => {
            allGenres.forEach(genreInfo => {
                if(el[0] == genreInfo.genre_id) {
                    el[0] = genreInfo.name;

                }
            })
            return `${el[0]}: ${el[1]}%`;
        }).join(", ");
        
        return (
            <SinglePlaylistOverview 
            key={el.playlist_id} 
            picture={el.cover} 
            title={el.playlist_title} 
            rank={calculatedRank} 
            duration={formattedDuration} 
            genres={genres}
            id={el.playlist_id}/>
        )
    })

    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <Container>
                <Spinner animation="border" />
            </Container>
        )
    }

    return (
        <Container className="authoredPlaylists">
            {userPlaylistsElements}
        </Container>
    )

}

export default UserPlaylists;