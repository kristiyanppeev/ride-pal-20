import { useEffect, useState } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Image from 'react-bootstrap/Image';
import { FILE_URL } from "../../../common/constants";
import './Avatar.css';
import Spinner from 'react-bootstrap/Spinner'
import AppError from "../../AppError/AppError";
import Loading from "../../Loading/Loading";
import ModalForm from './ModalForm/ModalForm';

const Avatar = ({ userInfo, userId, loggedUserId }) => {

    const [avatar, setAvatar] = useState(null);
    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);
    const [modalShow, setModalShow] = useState(false);

    useEffect(() => {
        if (userInfo) {
            setAvatar(userInfo.profile_pic);
        }
    }, [userInfo])

    if (error) {
        return (
            <div>
                <Container>
                    <AppError message={error} />
                </Container>
            </div>
        )
    }
    if (!userInfo || loading) {
        return (
            <div>
                <Container>
                    <Loading>
                        <Spinner animation="border" />
                    </Loading>
                </Container>
            </div>
        )
    }

    return (
        <Container className='username-title-avatar' >
            {modalShow ? <ModalForm show={modalShow} onHide={() => setModalShow(false)} setLoading={setLoading} setAvatar={setAvatar} setError={setError} /> : null}
            <Row>
                {loggedUserId == userId ?
                    <Col sm={3} className='uploadform' onClick={() => setModalShow(true)}>
                        <Image className='avatar' src={`${FILE_URL}${avatar}`} alt='avatar' roundedCircle />
                        <div className="middle">
                            <div className="text">Update avatar</div>
                        </div>
                    </Col> :
                    <Col sm={3}>
                        <Image className='avatar' src={`${FILE_URL}${avatar}`} alt='avatar' roundedCircle />
                    </Col>}
                <Col sm={9} className='username-title'>
                    <h1>{`${userInfo.username}`}</h1>
                </Col>
            </Row>

        </Container>
    )
}

export default Avatar;