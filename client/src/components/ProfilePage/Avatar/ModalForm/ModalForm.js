import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Container, Row, Col } from "react-bootstrap";
import { useEffect, useState } from "react";
import { BASE_URL } from '../../../../common/constants';
import { getToken } from '../../../../providers/AuthContext';



const ModalForm = ({show, onHide, setLoading, setAvatar, setError }) => {

    const [file, setFile] = useState(false);

    const uploadFile = (event) => {
        event.preventDefault();

        if(!file) {
            onHide();
            return null;
        }

        if (!file.name.match(/\.(jpg|jpeg|png)$/)) {
            alert("image must be .jpg, ,jpeg, or .png format.")
            return;
        }

        const imageFile = new FormData();
        imageFile.set('avatar', file);
        setLoading(true);

        fetch(`${BASE_URL}/users/avatar`, {
            method: 'PUT',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            },
            body: imageFile
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw new Error(res.error)
                }
                const newAvatarName = res.message.split(' ')[2];
                setAvatar(newAvatarName);
                onHide();
            })
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));
    }

    return (
        <Modal
            show={show} onHide={onHide} 
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
              Update avatar
            </Modal.Title>
          </Modal.Header>
            <Modal.Body>
                <Row>
                    <Form onSubmit={uploadFile} >
                        <Form.Group>
                            <Form.File id="exampleFormControlFile1" onChange={(e) => setFile(e.target.files[0])} />
                        </Form.Group>
                        <Button variant="primary" type="submit" className="uploadbutton" >Upload</Button>
                    </Form>
                </Row>
            </Modal.Body>
        </Modal>
    );
}

export default ModalForm;