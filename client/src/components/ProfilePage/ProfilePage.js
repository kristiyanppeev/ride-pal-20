import { useContext, useEffect, useState } from 'react';
import AppError from '../AppError/AppError';
import Avatar from './Avatar/Avatar';
import { Container } from "react-bootstrap";
import Spinner from 'react-bootstrap/Spinner';
import { BASE_URL } from '../../common/constants';
import AuthContext from '../../providers/AuthContext';
import UserPlaylists from './UserPlaylists/UserPlaylists';
import './ProfilePage.css'

const ProfilePage = ({ match }) => {
    const userId = match.params.id;

    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [userInfo, setUserInfo] = useState(null);
    const { isLoggedIn, user } = useContext(AuthContext);

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/users/${userId}`)
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw new Error(res.error)
                }
                setUserInfo(res);
            })
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));
    }, []);

    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <Container>
                <Spinner animation="border" />
            </Container>
        )
    }

    return (
        <Container className="userProfile">
            <Avatar userInfo={userInfo} userId={userId} loggedUserId={user.sub} />
            <UserPlaylists userId={userId} />
        </Container>
    )
}

export default ProfilePage;