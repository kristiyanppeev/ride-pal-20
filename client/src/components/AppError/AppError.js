import propTypes from 'prop-types';
import './AppError.css';

// eslint-disable-next-line react/prop-types
const AppError = ({ message }) => {

    return (
      <div className="AppError">
        <h1>{message}</h1>
      </div>
    );
};

AppError.propTypes = {
    message: propTypes.string.isRequired
}

export default AppError;