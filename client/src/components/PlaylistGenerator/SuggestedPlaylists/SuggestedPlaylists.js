import { useEffect, useState } from 'react';
import AppError from '../../AppError/AppError';
import Loading from '../../Loading/Loading';
import SinglePlaylistOverview from '../../SinglePlaylistOverview/SinglePlaylistOverview';
import Spinner from 'react-bootstrap/Spinner';
import { Container, Row, Col } from "react-bootstrap";
import { BASE_URL } from '../../../common/constants';
import { SUGGESTED_PLAYLISTS_PAGE_SIZE } from '../../../common/constants.js';
import './SuggestedPlaylists.css';

const SuggestedPlaylists = ({ travelDurationAsSeconds }) => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [page, setPage] = useState(0);
    const [playlists, setPlaylists] = useState([]);
    
    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/users/playlists/getall/?duration=${travelDurationAsSeconds}&page=${page}&pageSize=${SUGGESTED_PLAYLISTS_PAGE_SIZE}`)
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw new Error(res.error)
                };

                res.playLists = res.playLists.filter(el => el.combinedGenresSongs.length !== 0);

                res.playLists.forEach((el) => {
                    fetch(`${BASE_URL}/users/artists/${el.combinedGenresSongs[0].artists_artist_id}`)
                        .then(res => res.json())
                        .then(res => {
                            if (res.error) {
                                throw new Error(res.error)
                            }
                            el.cover = res.picture;

                        })
                        .catch(err => setError(err.message))

                })

                res.playLists.forEach((el) => {
                    fetch(`${BASE_URL}/users/${el.users_id}`)
                        .then(res => res.json())
                        .then(res => {
                            if (res.error) {
                                throw new Error(res.error)
                            }
                            el.author = res.username;
                        })
                        .catch(err => setError(err.message))
                })

                res.playLists.forEach(playlist => {
                    Object.entries(playlist.genres).forEach(el => {
                        if (el[1] > 0) {
                            fetch(`${BASE_URL}/users/genres/${el[0]}`)
                                .then(response => response.json())
                                .then(response => {
                                    if (response.error) {
                                        throw new Error(response.error)
                                    };
                                    playlist.genres[response.genre_id] = `${response.name}: ${playlist.genres[response.genre_id]}%`
                                    if (!playlist.genresNames) {
                                        playlist.genresNames = response.name;
                                    } else {
                                        playlist.genresNames = `${playlist.genresNames}, ${response.name}`
                                    }
                                })
                                .catch(err => setError(err.message))
                                .finally(() => setPlaylists([...res.playLists]));
                        }
                    })

                })
            })
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));

    }, [page, travelDurationAsSeconds])

    const displayedPlaylists = playlists.map(el => {
        const hours = Math.floor(el.duration / 3600);
        const minutes = Math.floor((el.duration % 3600) / 60);
        const formattedDuration = `${hours}:${minutes} min`
        el.tags = el.tags.map(el => el.toUpperCase());
        const calculatedRank = `${(el.rank/100000).toFixed(1)}/10`;
        const genresWithPercents = Object.values(el.genres).filter(el => el != 0).join(', ');
        return (
            <SinglePlaylistOverview 
            key={el.playlist_id} 
            picture={el.cover} 
            title={el.playlist_title} 
            genres={genresWithPercents} 
            duration={formattedDuration}
            rank={calculatedRank}
            id={el.playlist_id}
             />
        )
    })

    const nextPage = () => {
        if(displayedPlaylists.length === SUGGESTED_PLAYLISTS_PAGE_SIZE) {
            setPage(page+1);
        }

    }

    const prevPage = () => {
        if(page !== 0) {
            setPage(page-1);
        }
    }

    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <div>
                <Container>
                    <Loading>
                        <Spinner animation="border" />
                    </Loading>
                </Container>
            </div>
        )
    }

    return (
        <Container className="suggestedPlaylists">
            <Row className="suggestedPlaylistsTitle">
                Suggested Playlists For Your Travel:
            </Row>
            <Row className="suggestedPlaylistsContent">
                {displayedPlaylists.length !== 0 ? displayedPlaylists : "There are no suggested playlists for your travel duration."}
            </Row>
            {displayedPlaylists.length !== 0 && <Row className="suggestedPlaylistsButtons">
                <Col disabled={true} className="suggestedPlaylistsButtonPrev" onClick={prevPage}>Previous</Col>
                <Col className="suggestedPlaylistsButtonNext" onClick={nextPage} >Next</Col>
            </Row>}
        </Container>
    )
}

export default SuggestedPlaylists;