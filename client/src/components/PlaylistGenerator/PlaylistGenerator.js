import { useState } from "react";
import SuggestedPlaylists from "./SuggestedPlaylists/SuggestedPlaylists";
import TravelInformation from "./TravelInformation/TravelInformation";
import { Button } from "react-bootstrap";
import './PlaylistGenerator.css'

const PlaylistGenerator = ({ history }) => {

    const [travelDurationAsSeconds, setTravelDurationAsSeconds] = useState(null);
    return (
        <div>
            <TravelInformation travelDurationAsSeconds={travelDurationAsSeconds} setTravelDurationAsSeconds={setTravelDurationAsSeconds} />
            {travelDurationAsSeconds && 
            <SuggestedPlaylists travelDurationAsSeconds={travelDurationAsSeconds} />}
            {travelDurationAsSeconds &&
            <h1 className="generateOwnPlaylistFooter">
                <Button variant="light" className="generateOwnPlaylistButton" onClick={() => history.push(`./generator/${travelDurationAsSeconds}`)} >Generate</Button> your own playlist.
            </h1>}
        </div>
    )
}

export default PlaylistGenerator;