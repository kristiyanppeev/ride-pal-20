import { useEffect, useState } from "react";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './TravelInformation.css'
import { Button, Container } from "react-bootstrap";
import Sidebar from "../../../common/Sidebar/Sidebar";
import { BASE_URL } from "../../../common/constants";
import Spinner from 'react-bootstrap/Spinner';
import Loading from "../../Loading/Loading";
import AppError from '../../AppError/AppError';
import { getToken } from "../../../providers/AuthContext";

const TravelInformation = ({ travelDurationAsSeconds, setTravelDurationAsSeconds }) => {

    const [popUpMessage, setPopUpMessage] = useState('');
    const [modalShow, setModalShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const [travelDuration, setTravelDuration] = useState(null);
    const [classOne, setClassOne] = useState('waypoints');
    const [classTwo, setClassTwo] = useState('waypoints');
    const [classThree, setClassThree] = useState('waypoints');
    const [numberOfWaypoints, setNumberOfWaypoints] = useState(0);
    const [travelInfo, setTravelInfo] = useState({
        startingPoint: '',
        waypointOne: '',
        waypointTwo: '',
        waypointThree: '',
        destination: ''
    })

    useEffect(() => {
        switch (+numberOfWaypoints) {
            case 0:
                setClassOne('waypoints');
                break;
            case 1:
                setClassOne('showWaypoint');
                setClassTwo('waypoints');

                break;
            case 2:
                setClassTwo('showWaypoint');
                setClassThree('waypoints');
                break;
            case 3:
                setClassThree('showWaypoint');
                break;
            default:
                setClassOne('showWaypoint');
                setClassTwo('showWaypoint');
                setClassThree('showWaypoint');
        }
    }, [numberOfWaypoints])

    const handleSubmit = event => {
        event.preventDefault();

        if(travelInfo.startingPoint.length === 0 || travelInfo.destination.length === 0) {
            setPopUpMessage("Starting and ending point you must have.")
            setModalShow(true);
            return;
        }

        const waypoints = Object.values(travelInfo).filter(el => el.length > 0).map((el, index) => `wp${index}=${el}`).join("&");

        setLoading(true);

        fetch(`${BASE_URL}/users/addresses?${waypoints}`, {
            headers: {
                Authorization: `Bearer ${getToken()}`,
            }
        })
        .then(res => res.json())
        .then(res => {
            if (res.error === "Please enter valid waypoints.") {
                setPopUpMessage("Valid waypoints please enter")
                setModalShow(true);
                return;
            }
            if (res.error) {
                throw new Error(res.error)
            }

            setTravelDurationAsSeconds(res.duration);

            const hours = Math.floor(res.duration/3600);
            const minutes= Math.floor((res.duration%3600)/60);

            setTravelDuration(`${hours} hours ${minutes} minutes`);

        })
        .catch(err => setError(err.message))
        .finally(() => setLoading(false));
    }

    const handleInputChange = (event) => {
        const { name, value } = event.target;

        travelInfo[name] = value;

        setTravelInfo({...travelInfo});
    };

    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <div>
                <Container>
                    <Loading>
                        <Spinner animation="border" />
                    </Loading>
                </Container>
            </div>
        )
    }

    return (
        <Form className="waypointsForm" onSubmit={handleSubmit}>
            {modalShow ? <Sidebar modalShow={modalShow} setModalShow={() => setModalShow(false)} popUpMessage={popUpMessage} /> : null}
            <Col>
                <Row className="addRemoveButtons">               
                    <img className="removeButton" alt="remove-button" onClick={() => { if (numberOfWaypoints > 0) { setNumberOfWaypoints(numberOfWaypoints - 1) } }} src={process.env.PUBLIC_URL + '/arrowUp.jpg'}/>
                    <img className="addButton" alt="add-button" onClick={() => { if (numberOfWaypoints < 3) { setNumberOfWaypoints(numberOfWaypoints + 1) } }} src={process.env.PUBLIC_URL + '/arrowDown.jpg'}/>
                </Row>
                <Row className='waypointInputs'>
                    <Form.Control className="showWaypoint" onChange={handleInputChange} value={travelInfo.startingPoint} name="startingPoint" placeholder="starting point" />

                    <Form.Control type="text" className={`${classOne}`} onChange={handleInputChange} value={travelInfo.waypointOne} name="waypointOne" placeholder={classOne === 'waypoints' ? null : "waypoint one"} />

                    <Form.Control type="text" className={`${classTwo}`} onChange={handleInputChange} value={travelInfo.waypointTwo} name="waypointTwo" placeholder={classTwo === 'waypoints' ? null : "waypoint two"} />

                    <Form.Control type="text" className={`${classThree}`} onChange={handleInputChange} value={travelInfo.waypointThree} name="waypointThree" placeholder={classThree === 'waypoints' ? null : "waypoint three"} />

                    <Form.Control className="showWaypoint" onChange={handleInputChange} value={travelInfo.destination} name="destination" placeholder="destination" />
                </Row>
                <Row>
                    <Button className="sendButton" variant="light" type="submit" >Send</Button>
                </Row>
                <Row>
                    {travelDuration && <div className="travelDuration" >Expected travel duration: {travelDuration}</div>}
                </Row>
            </Col>
        </Form>
    )
}

export default TravelInformation;