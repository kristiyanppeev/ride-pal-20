import { Container, Row, Col, DropdownButton } from 'react-bootstrap';
import { withRouter } from 'react-router-dom';
import { BASE_URL, FILE_URL } from "../../common/constants";
import AppError from "../AppError/AppError";
import Loading from '../Loading/Loading';
import Spinner from 'react-bootstrap/Spinner';
import { useContext, useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';
import AuthContext, { getToken } from '../../providers/AuthContext';
import './Header.css';
import Dropdown from 'react-bootstrap/Dropdown';
import Image from 'react-bootstrap/Image';
import ButtonGroup from 'react-bootstrap/ButtonGroup'

const Header = ({ location, history }) => {

    const auth = useContext(AuthContext);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);

    const logout = () => {
        setLoading(true);
        fetch(`${BASE_URL}/users/logout`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw new Error(res.message);
                }
                // setModalShow(true);
                // setMessage('You have successfully logged out.')
                localStorage.removeItem('token');
                auth.setLoginState({
                    user: null,
                    isLoggedIn: false,
                });
            })
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));
    }

    if (location.pathname === "/login" || location.pathname === "/register" || location.pathname.substring(0, 10) === "/playlist/") return null;

    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <div>
                <Container>
                    <Loading>
                        <Spinner animation="border" />
                    </Loading>
                </Container>
            </div>
        )
    }

    return (
        <Container fluid className="header">
            <Row>
                <Col>
                    <Image className="homeButton" src="/yoda3.png" onClick={() => history.push('/')} />
                </Col>
                <Col>
                <ButtonGroup className="navigationButtons">
                    {auth.isLoggedIn
                        ? <Button variant="link" className="auth-button" onClick={logout}>Logout</Button>
                        : <Link className="auth-button" to={'/login'}><Button variant="link">Sign in</Button></Link>
                    }
                    <DropdownButton as={ButtonGroup} variant="link" title="" id="bg-nested-dropdown">
                        <Dropdown.Item onClick={() => history.push('/playlists')}>Browse</Dropdown.Item>
                        {auth.isLoggedIn && (auth.user.role === "admin" ? <Dropdown.Item onClick={() => history.push('/admins')}>Admin</Dropdown.Item> : null)}
                        {auth.isLoggedIn ? <Dropdown.Item onClick={() => history.push('/travel')}>Travel</Dropdown.Item> : null}
                        {auth.isLoggedIn ? <Dropdown.Item onClick={() => history.push(`/profile/${auth.user.sub}`)}>Profile</Dropdown.Item> : null}
                    </DropdownButton>
                </ButtonGroup>
                </Col>
            </Row>
        </Container>
    )
}

export default withRouter(Header);

{/* <Button variant="light" className="auth-button" onClick={() => history.push('/playlists')}>Browse</Button>
            {auth.isLoggedIn && (auth.user.role === "admin"
                ? <Button variant="light" className="auth-button" onClick={() => history.push('/admins')}>Admin</Button>
                : null)
            }
            {auth.isLoggedIn
                ? <Button variant="light" className="auth-button" onClick={() => history.push('/travel')}>Travel</Button>
                : null
            } */}

        //     <Col>
        //     <Dropdown>
        //         <Dropdown.Toggle variant="light" id="dropdown-basic">
        //             More actions
        //         </Dropdown.Toggle>

        //         <Dropdown.Menu>
        //             <Dropdown.Item href="#/action-1" onClick={() => history.push('/playlists')}>Browse</Dropdown.Item>
        //             {auth.isLoggedIn && (auth.user.role === "admin" ? <Dropdown.Item href="#/action-2" onClick={() => history.push('/admins')}>Admin</Dropdown.Item> : null)}
        //             {auth.isLoggedIn ? <Dropdown.Item href="#/action-3" onClick={() => history.push('/travel')}>Travel</Dropdown.Item> : null}
        //             {auth.isLoggedIn ? <Dropdown.Item href="#/action-3" onClick={() => history.push(`/profile/${auth.user.sub}`)}>Profile</Dropdown.Item> : null}
        //         </Dropdown.Menu>
        //     </Dropdown>
        // </Col>
        // <Col>
        //     {auth.isLoggedIn
        //         ? <Button variant="light" className="auth-button" onClick={logout}>Logout</Button>
        //         : <Link className="auth-button" to={'/login'}><Button variant="light">Sign in</Button></Link>
        //     }
        // </Col>