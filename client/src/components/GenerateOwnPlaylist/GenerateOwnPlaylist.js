import InputGroup from 'react-bootstrap/InputGroup';
import { Button, Container, FormControl, Row, Image } from "react-bootstrap";
import './GenerateOwnPlaylist.css'
import { useEffect, useState } from 'react';
import AppError from '../AppError/AppError';
import Chip from '@material-ui/core/Chip';
import Spinner from 'react-bootstrap/Spinner';
import { BASE_URL } from '../../common/constants';
import Sidebar from '../../common/Sidebar/Sidebar';
import { getToken } from '../../providers/AuthContext';
import { v4 as uuidv4 } from 'uuid';

const GenerateOwnPlaylist = ({ match, history }) => {

    const travelDurationAsSeconds = match.params.duration;
    const hours = Math.floor(travelDurationAsSeconds / 3600);
    const minutes = Math.floor((travelDurationAsSeconds % 3600) / 60);

    const [popUpMessage, setPopUpMessage] = useState('');
    const [modalShow, setModalShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [genres, setGenres] = useState([]);
    const [formValues, setFormValues] = useState({});
    const [generatedPlaylist, setGeneratedPlaylist] = useState(null);
    const [tagChips, setTagChips] = useState([]);
    const [isTagInvalid, setIsTagInvalid] = useState(false);
    const [playlistAdditionalInfoValid, setPlaylistAdditionalInfoValid] = useState(false);
    const [playlistAdditionalInfo, setPlaylistAdditionalInfo] = useState({
        title: {
            placeholder: "Playlist title",
            value: '',
            className: "generatePlaylistFormInputs",
            type: 'text',
            validate: (value) => value.length > 4 && value.length < 50 && /^[A-Za-z0-9, ']+$/.test(value),
            valid: false,
            errMessage: 'Title must be between 5 and 50 symbols without special caracters',
            errMessagePopUp: 'Title Invalid'
        },
        tags: {
            placeholder: 'tags',
            value: '',
            valuesArray: [],
            className: "generatePlaylistFormInputs",
            type: "text",
            validate: (value) => /^[A-Za-z0-9, ]+$/.test(value) && value.replace(/ /g,'').length > 3 && value.length < 15 || value.trim() === "",
            valid: true,
            errMessage: 'Maximum 5 tags between 4 and 15 symbols without special characters are allowed',
            errMessagePopUp: 'Tags Invalid'
        },
    });

    useEffect(() => {
        setLoading(true);
        fetch(`${BASE_URL}/users/allgenres`)
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw new Error(res.error)
                }

                setGenres(res);
            })
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));
    }, []);

    useEffect(() => {
        const genresObj = Object.values(genres).reduce((acc, el) => {
            return {
                ...acc,
                [el.name]: ''
            }
        }, {})

        setFormValues(genresObj);
    }, [genres])


    const handleInputChange = (event) => {
        setFormValues({ ...formValues, [event.target.title]: event.target.value });
    };

    const handlePlaylistAdditionalInfoChange = (event) => {
        const { value, title } = event.target;

        playlistAdditionalInfo[title].value = value;
        playlistAdditionalInfo[title].valid = playlistAdditionalInfo[title].validate(value);

        setPlaylistAdditionalInfo({ ...playlistAdditionalInfo })

        const isAdditionalInfoValid = Object.values(playlistAdditionalInfo).every(elem => elem.valid);

        setPlaylistAdditionalInfoValid(isAdditionalInfoValid);
    };

    const deleteTag = (index) => {
        playlistAdditionalInfo.tags.valuesArray.splice(index, 1)
        setPlaylistAdditionalInfo({ ...playlistAdditionalInfo })
        updateGenreChips();
    }

    const updateGenreChips = () => {
        const chips = playlistAdditionalInfo.tags.valuesArray.map((el, index) =>
            <Chip
                key={uuidv4()}
                className="chipTags"
                label={el}
                onDelete={() => deleteTag(index)}
                variant="outlined"
            />);

        setTagChips(chips);
    }

    const handleKeyPress = event => {
        const x = event.which || event.keyCode;
        const inputValue = playlistAdditionalInfo.tags.value.trim(); //trim deleted
        if (x === 32 && inputValue !== "") {
            if (playlistAdditionalInfo.tags.valuesArray.length > 4) {
                setIsTagInvalid(true);
                return;
            }
            if (playlistAdditionalInfo.tags.valid === false) {
                setIsTagInvalid(true);
            } else {
                setIsTagInvalid(false);
                playlistAdditionalInfo.tags.valuesArray.push(inputValue);
                playlistAdditionalInfo.tags.value = '';
                updateGenreChips();
            }
        }
    }

    const formInputs = genres.map(el => {

        return (
            <InputGroup key={el.genre_id}>
                <FormControl
                    className="generatePlaylistFormInputs"
                    placeholder={el.name}
                    aria-label={el.name}
                    aria-describedby="basic-addon2"
                    value={formValues[el.name]}
                    onChange={handleInputChange}
                    title={el.name}
                />
                <InputGroup.Append className="generatePlaylistFormInputs">
                    <Button variant="outline-secondary" className="percentButton" onClick={handleInputChange} value={25} title={el.name} >25%</Button>
                    <Button variant="outline-secondary" className="percentButton" onClick={handleInputChange} value={50} title={el.name} >50%</Button>
                    <Button variant="outline-secondary" className="percentButton" onClick={handleInputChange} value={100} title={el.name} >100%</Button>
                </InputGroup.Append>
            </InputGroup>
        )
    })

    const submitFormValues = () => {

        Object.values(playlistAdditionalInfo).forEach(el => {
            if (el.valid === false) {
                setPopUpMessage(el.errMessagePopUp);
            }
        })

        if (playlistAdditionalInfo.tags.valid === false) {
            setIsTagInvalid(true);
        }

        const percentValuesSum = Object.values(formValues).reduce((acc, el) => {
            acc += +el;
            return acc;
        }, 0)

        if (percentValuesSum !== 100) {
            setPopUpMessage("Must equal 100 sum of genre percents.")
            setModalShow(true);
            return;
        }

        if (!playlistAdditionalInfoValid) {
            setModalShow(true);
            return;
        }

        const queryString = Object.entries(formValues).filter(el => el[1] !== "").map(el => el[0] === "Rap/Hip Hop" ? ["Rap", el[1]].join('=') : el.join('=')).join('&');

        fetch(`${BASE_URL}/users/playlists/${travelDurationAsSeconds}?${queryString}`, {
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        })
            .then(res => res.json())
            .then(res => {
                if (res.error) {
                    throw new Error(res.error)
                }

                setGeneratedPlaylist(res);

            })
            .catch(err => setError(err.message))
            .finally(() => setLoading(false));
    };

    useEffect(() => {
        if (generatedPlaylist) {
            setLoading(true);

            const genresPercents = genres.reduce((acc, genre) => {

                Object.entries(formValues).forEach((el) => {
                    if (genre.name === el[0]) {
                        acc = {
                            ...acc,
                            [genre.genre_id]: el[1] || "0"
                        }
                    }
                })
                return acc;
            }, {});

            const tagsInputValue = playlistAdditionalInfo.tags.value.trim(); //trim deleted

            if (tagsInputValue !== "") {
                playlistAdditionalInfo.tags.valuesArray.push(tagsInputValue);
            }

            const playlistInfo = {
                playListTitle: playlistAdditionalInfo.title.value,
                tags: playlistAdditionalInfo.tags.valuesArray,
                genres: genresPercents,
                combinedGenresSongsDuration: generatedPlaylist.combinedGenresSongsDuration,
                combinedGenresSongs: generatedPlaylist.combinedGenresSongs
            }

            fetch(`${BASE_URL}/users/playlists`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${getToken()}`,
                },
                body: JSON.stringify(playlistInfo),
            })
                .then(res => res.json())
                .then(res => {
                    if (res.error) {
                        throw new Error(res.error)
                    }

                    history.push(`/playlist/${res.playlist_id}`)

                })
                .catch(err => setError(err.message))
                .finally(() => setLoading(false));


        }
    }, [generatedPlaylist]);

    if (error) {
        return (
            <AppError message={error} />
        )
    }

    if (loading) {
        return (
            <Container>
                <Spinner animation="border" />
            </Container>
        )
    }

    return (
        <Container className="generatePlaylistForm">
            {modalShow ? <Sidebar modalShow={modalShow} setModalShow={() => setModalShow(false)} popUpMessage={popUpMessage} /> : null}
            <p className="generateOwnPlaylistTravelDuration">Travei duration: {hours}:{minutes} hrs</p>
            <FormControl
                className="generatePlaylistFormInputs"
                placeholder="Title" value={playlistAdditionalInfo.title.value}
                onChange={handlePlaylistAdditionalInfoChange}
                title='title' />
            {!playlistAdditionalInfo.title.valid && popUpMessage !== "" ?
                <Row className="additionalInfoLoginErrors"><span>{playlistAdditionalInfo.title.errMessage}</span> </Row>
                : null}
            <Row className="playlistTags">
                <div className='chipTags'>{tagChips}</div>
                <FormControl
                    className="chipInput"
                    placeholder="Tags"
                    value={playlistAdditionalInfo.tags.value}
                    onKeyPress={handleKeyPress}
                    onChange={handlePlaylistAdditionalInfoChange}
                    title='tags' />
            </Row>
            {isTagInvalid && <Row className="additionalInfoLoginErrors"><span>{playlistAdditionalInfo.tags.errMessage}</span></Row>}
            {formInputs}
            <Button variant="light" className="generatePlaylistButton" onClick={submitFormValues} >Generate</Button>
        </Container >
    )

}

export default GenerateOwnPlaylist;