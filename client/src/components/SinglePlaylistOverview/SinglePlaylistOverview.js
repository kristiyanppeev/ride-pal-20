import { Container, Col, Row, Image } from "react-bootstrap";
import { useHistory } from "react-router";
import './SinglePlaylistOverview.css'


const SinglePlaylistOverview = ({ picture, title, genres, rank, duration, id }) => {
    const history = useHistory();

    return (
        <Container className="singlePlaylistOverview" onClick={() => history.push(`/playlist/${id}`)}>
            <Row>
            <Col  sm={3} className="singlePlaylistImageCont">
            <Image className='singlePlaylistImage' src={picture} roundedCircle />
            </Col>
            <Col sm={9} className="singlePlaylistDetails">
            <Row>
                <h1 className="textDetails">{title}</h1>
            </Row>
            <Row>
                <h2 className="textDetails">{genres}</h2>
            </Row>
            <Row>
                <h2 className="textDetails">{duration}</h2>
            </Row>
            <Row>
                <h2 className="textDetails">Rating: {rank}</h2>
            </Row>
            </Col>
            </Row>
        </Container>
    )
};

export default SinglePlaylistOverview;