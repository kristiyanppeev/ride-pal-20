import { useEffect, useState } from 'react';
import { BASE_URL } from '../../../common/constants.js'
import AppError from '../../AppError/AppError.js';
import Spinner from 'react-bootstrap/Spinner';
import { Container } from "react-bootstrap";
import Loading from '../../Loading/Loading.js';
import './GetPlaylists.css';
import SinglePlaylistOverview from '../../SinglePlaylistOverview/SinglePlaylistOverview.js';

const GetPlaylists = () => {

  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const [playlists, setPlaylists] = useState([]);


  useEffect(() => {
    setLoading(true);
    fetch(`${BASE_URL}/users/playlists/getall?page=0&pageSize=10`)
      .then(res => res.json())
      .then(res => {
        if (res.error) {
          throw new Error(res.error)
        }

        if (res.playLists.length === 0) {
          // there are no playlists to show
          return;
        }
        // if(res.playLists.combinedGenreSongs.length === 0) {
        //   return;
        // }

        res.playLists.forEach(el => {
          console.log(el.combinedGenresSongs)
          fetch(`${BASE_URL}/users/artists/${el.combinedGenresSongs[0].artists_artist_id}`)
            .then(res => res.json())
            .then(res => {
              if (res.error) {
                throw new Error(res.error)
              }
              el.cover = res.picture;

            })
            .catch(err => setError(err.message))

        })

        res.playLists.forEach(playlist => {

          Object.entries(playlist.genres).forEach(el => {
            if (el[1] > 0) {
              fetch(`${BASE_URL}/users/genres/${el[0]}`)
                .then(response => response.json())
                .then(response => {
                  if (response.error) {
                    throw new Error(response.error)
                  };

                  if(!playlist.genresNames) {
                    playlist.genresNames = response.name;
                  } else {
                    playlist.genresNames = `${playlist.genresNames}, ${response.name}`
                  }
                })
                .catch(err => setError(err.message))
                .finally(() => setPlaylists([...res.playLists]));
            }
          })

        })

      })
      .catch(err => setError(err.message))
      .finally(() => setLoading(false));
  }, [])




  if (error) {
    return (
      <AppError message={error} />
    )
  }

  if (loading) {
    return (
      <div>
        <Container>
          <Loading>
            <Spinner animation="border" />
          </Loading>
        </Container>
      </div>
    )
  }

  return (
    <div>
      <p>Find playlists</p>
      {playlists.map(playlist => console.log(playlist))}

      
      {/* {playlists.map(playlist => <SinglePlaylistOverview picture={playlist.title}/>)} */}
    </div>
  )
}

export default GetPlaylists;
