import { useEffect, useState } from "react";
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import './Playlists.css'
import { Button, Container } from "react-bootstrap";
import Sidebar from "../../common/Sidebar/Sidebar";
import { BASE_URL, SUGGESTED_PLAYLISTS_PAGE_SIZE_LAPTOP } from "../../common/constants";
import Spinner from 'react-bootstrap/Spinner';
import Loading from "../Loading/Loading";
import AppError from '../AppError/AppError';
import { getToken } from "../../providers/AuthContext";
import SinglePlaylistOverview from '../../components/SinglePlaylistOverview/SinglePlaylistOverview';

const Playlists = ({ history }) => {

  const [popUpMessage, setPopUpMessage] = useState('');
  const [modalShow, setModalShow] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [playlists, setPlaylists] = useState(null);
  const [startSearch, setStartSearch] = useState(false);

  const [genreBtnColor, setGenreBtnColor] = useState({
    metal: '#6c757d',
    rock: '#6c757d',
    pop: '#6c757d',
    rap: '#6c757d',
  });

  const [searchData, setSearchData] = useState({
    title: '',
    duration: '',
    durationHours: '',
    durationMinutes: '',
    pop: '',
    rock: '',
    metal: '',
    rap: '',
    page: 0,
    pageSize: SUGGESTED_PLAYLISTS_PAGE_SIZE_LAPTOP
  })

  useEffect(() => {
    setLoading(true);

    // Group the queries in titleDuration and genres for easier formatting
    const urlQueries = Object.keys(searchData).reduce((acc, query) => {

      if (searchData[query] === '') return acc;

      if (query === 'title' || query === 'duration' || query === 'durationHours' || query === 'durationMinutes' || query === 'page' || query === 'pageSize') {
        
        if (query === 'duration' || query === 'durationHours' || query === 'durationMinutes') {
          
          if (query === 'duration') {
            const seconds = (+searchData.durationHours * 60 * 60) + (+searchData.durationMinutes * 60);
            acc.titleDuration.push(`${query}=${seconds}`);
          }

        } else {
          acc.titleDuration.push(`${query}=${searchData[query]}`);
        }
      } else {
        acc.genres.push(`${searchData[query]}`);
      }
      return acc;
    }, { titleDuration: [], genres: [] });
    // console.log(urlQueries)

    // Create parts of the URL string
    let urlCompiled = Object.keys(urlQueries)
      .map((group) => {
        if (group === 'titleDuration') return urlQueries.titleDuration.join('&');
        if (group === 'genres') return 'genres=' + urlQueries.genres.join(';');
      });

    if (urlQueries.genres.length === 0) {
      urlCompiled = urlCompiled[0];
    } else {
      urlCompiled = urlCompiled.join('&');
    }

    // Get all info about genres
    fetch(`${BASE_URL}/users/genres/get`, {
      headers: {
        Authorization: `Bearer ${getToken()}`,
      }
    })
      .then(res => res.json())
      .then(res => {
        //  Handle DB errors
        if (res.error) {
          setPopUpMessage("Faulty our server is. Sorry we are!")
          setModalShow(true);
          return;
        }

        const genresNamesAndIds = {};
        const genresIdsAndNames = {};

        // Create objects with Names and Ids of the genres in a useful way
        res.genres.map(genre => {
          let name = genre.name.toLowerCase();
          if (genre.name === 'Rap/Hip Hop') name = 'rap';
          genresNamesAndIds[name] = genre.genre_id;
          genresIdsAndNames[genre.genre_id] = name;
        });

        // Create the URL to get the requested playlists
        const urlToSend = urlCompiled
          .replace('rap', `${genresNamesAndIds.rap}`)
          .replace('metal', `${genresNamesAndIds.metal}`)
          .replace('pop', `${genresNamesAndIds.pop}`)
          .replace('rock', `${genresNamesAndIds.rock}`);

        // Get the playlists
        // console.log(`${BASE_URL}/users/playlists/getall?${urlToSend}`)

        fetch(`${BASE_URL}/users/playlists/getall?${urlToSend}`, {
          headers: {
            Authorization: `Bearer ${getToken()}`,
          }
        })
          .then(res => res.json())
          .then(res => {
            // Handle errors that are not from the DB
            if (res.error && !res.error.includes('sql')) {
              setPopUpMessage("Correct your choices, you must!")
              setModalShow(true);
              return;
            } else if (res.error) {
              throw new Error(res.error)
            }

            // For every playlist, fill image and genres(in a suitable, string format 'metal:20%')
            Promise.all(res.playLists.map(async (list) => {

              await fetch(`${BASE_URL}/users/${list.users_id}`)
                .then(res => res.json())
                .then(res => {
                  if (res.error) {
                    throw new Error(res.error)
                  }
                  list.user = res.username;
                })
                .catch(err => setError(err.message))

              const id = list.combinedGenresSongs[0].artists_artist_id;
              // Get the artist, for the first song of a playlist (holds the images)
              await fetch(`${BASE_URL}/users/artists/${id}`, {
                headers: {
                  Authorization: `Bearer ${getToken()}`,
                }
              })
                .then(res => res.json())
                .then(res => {
                  if (res.error) {
                    throw new Error(res.error)
                  }
                  // Add picture to the playlist
                  list.picture = res.picture;

                  // Convert duration seconds in a readable format
                  const hours = Math.floor(list.duration / 3600);
                  const minutes = Math.floor((list.duration % 3600) / 60);

                  list.duration = `${hours} hours ${minutes} minutes`;

                  // Convert tags to a string
                  list.tags = list.tags.map(t => t.toUpperCase()).join(', ');

                  // Add average rank
                  list.rank = `${(list.rank / 100000).toFixed(1)}/10`

                  // While we are in the playlist, we fill the genres as a string 'metal:20%'
                  list.genres = Object.keys(list.genres)
                    .filter(genre => list.genres[genre] > 0)
                    .reduce((acc, genre) => {
                      const genreWithPercent = `${genresIdsAndNames[genre]}: ${list.genres[genre]}%`
                      acc.push(genreWithPercent);
                      return acc;
                    }, [])
                    .join(', ');
                })
              return list;
            }))
              .then((values) => {
                // Set the updated playlists from the Promise.all map
                setPlaylists(values);
              })
          })
      })
      .catch(err => setError(err.message))
      .finally(() => {
        setLoading(false);
      });
  }, [startSearch]);




  // console.log(playlists)
  const handleInputChange = (event) => {
    let { name, value } = event.target;

    if (name === 'durationHours' || name === 'durationMinutes') {
      searchData.duration = value;
    }

    searchData[name] = value;

    setSearchData({ ...searchData });
  };

  const nextPage = () => {
    if (playlists.length === SUGGESTED_PLAYLISTS_PAGE_SIZE_LAPTOP) {
      const newPage = searchData.page + 1;
      setSearchData({ ...searchData, page: newPage });
      setStartSearch(!startSearch);
    }
  }

  const prevPage = () => {
    const newPage = searchData.page - 1;
    if (newPage >= 0) {
      setSearchData({ ...searchData, page: newPage });
      setStartSearch(!startSearch);
    }
  }

  const toggleBtnColor = (event) => {
    const genreName = event.target.name;
    // console.log(genreBtnColor[genreName])
    const color = genreBtnColor[genreName] === 'white' ? '#6c757d' : 'white';
    genreBtnColor[genreName] = color;
    setGenreBtnColor({ ...genreBtnColor })
  }

  if (error) {
    return (
      <AppError message={error} />
    )
  }

  if (loading) {
    return (
      <div>
        <Container>
          <Loading>
            <Spinner animation="border" />
          </Loading>
        </Container>
      </div>
    )
  }

  return (
    <div className="waypointsForm">
      {modalShow ? <Sidebar modalShow={modalShow} setModalShow={() => setModalShow(false)} popUpMessage={popUpMessage} /> : null}
      <Col>
        <Row className='waypointInputs'>
          <Form.Control type="text" className="showWaypoint" onChange={handleInputChange} value={searchData.title} name="title" placeholder="title" />

          <Row>
            <Col>
              <Form.Control type="text" id='durationHours' className="showWaypoint" onChange={handleInputChange} value={searchData.durationHours} name="durationHours" placeholder="hours" />
            </Col>
            <Col>
              <Form.Control type="text" id='durationMinutes' className="showWaypoint" onChange={handleInputChange} value={searchData.durationMinutes} name="durationMinutes" placeholder="minutes" />
            </Col>
          </Row>
        </Row>

        <Row className="genreBtns">
          <Col >
            <Button style={{ color: genreBtnColor.pop }} type="button" className="genreButton" onClick={(e) => { handleInputChange(e); toggleBtnColor(e) }} value={searchData.pop === '' ? 'pop' : ''} name="pop">{
              <img className="addGenre" alt="add-button" src={process.env.PUBLIC_URL + '/arrowDown.jpg'} />}Pop</Button>
          </Col>
          <Col>
            <Button style={{ color: genreBtnColor.metal }} type="button" className="genreButton" onClick={(e) => { handleInputChange(e); toggleBtnColor(e) }} value={searchData.metal === '' ? 'metal' : ''} name="metal">{
              <img className="addGenre" alt="add-button" src={process.env.PUBLIC_URL + '/arrowDown.jpg'} />}Metal</Button>
          </Col>
          <Col>
            <Button style={{ color: genreBtnColor.rock }} type="button" className="genreButton" onClick={(e) => { handleInputChange(e); toggleBtnColor(e) }} value={searchData.rock === '' ? 'rock' : ''} name="rock">{
              <img className="addGenre" alt="add-button" src={process.env.PUBLIC_URL + '/arrowDown.jpg'} />}Rock</Button>
          </Col>
          <Col>
            <Button style={{ color: genreBtnColor.rap }} type="button" className="genreButton" onClick={(e) => { handleInputChange(e); toggleBtnColor(e) }} value={searchData.rap === '' ? 'rap' : ''} name="rap">{
              <img className="addGenre" alt="add-button" src={process.env.PUBLIC_URL + '/arrowDown.jpg'} />}Rap</Button>
          </Col>
        </Row>

        <Row>
          <Button className="sendButton" variant="light" type="submit" onClick={() => setStartSearch(!startSearch)}>Find</Button>
        </Row>
        <Row className="searchResults">
          {playlists && playlists.map(playList => <SinglePlaylistOverview key={playList.playlist_id} id={playList.playlist_id} picture={playList.picture} title={playList.playlist_title} genres={playList.genres} rank={playList.rank} duration={playList.duration} />)}
          {(Array.isArray(playlists) && playlists.length === 0) ? 'No more playlists to show.' : null}
        </Row>
        {playlists && <Row className="suggestedPlaylistsButtons">
          <Col type="submit" disabled={true} className="suggestedPlaylistsButtonPrev" onClick={(event) => prevPage(event)}>Previous</Col>
          <Col type="submit" className="suggestedPlaylistsButtonNext" onClick={(event) => nextPage(event)} >Next</Col>
        </Row>}
      </Col>
    </div>
  )
}

export default Playlists;
