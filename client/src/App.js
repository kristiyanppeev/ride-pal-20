import './App.css';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import { useState } from 'react';
import Signin from './components/Signin/Signin';
import Header from './components/Header/Header';
import Homepage from './components/Homepage/Homepage';
import PlaylistGenerator from './components/PlaylistGenerator/PlaylistGenerator';
import NotFound from './components/NotFound/NotFound'
import SinglePlaylistDetailed from './components/SinglePlaylistDetailed/SinglePlaylistDetailed';
import GenerateOwnPlaylist from './components/GenerateOwnPlaylist/GenerateOwnPlaylist';
import Playlists from './components/Playlists/Playlists';
import AdminsPanel from './components/Admins/AdminsPanel';
import ProfilePage from './components/ProfilePage/ProfilePage';
import GuardedRoute from './providers/GuardedRoute'

function App() {
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken())
  });

  return (
    <div className="App">
      <BrowserRouter>
        <AuthContext.Provider value={{ ...authValue, setLoginState: setAuthValue }}>
          <Header />
          <Switch>
            <Redirect path="/" exact to="/home" />
            <Route path="/home" component={Homepage} />
            <Route path="/login" component={Signin} />
            <Route path="/register" component={Signin} />
            <GuardedRoute path="/travel" isLoggedIn={authValue.isLoggedIn} component={PlaylistGenerator} />
            <GuardedRoute path="/profile/:id" isLoggedIn={authValue.isLoggedIn} component={ProfilePage} />
            <GuardedRoute path="/generator/:duration" isLoggedIn={authValue.isLoggedIn} component={GenerateOwnPlaylist} />
            <Route path="/playlists" exact component={Playlists} />
            <Route path="/playlist/:id" exact component={SinglePlaylistDetailed} />
            <GuardedRoute path="/admins" isLoggedIn={authValue.isLoggedIn && authValue.user.role === "admin"} component={AdminsPanel} />
            <Route path="*" component={NotFound} />
          </Switch>
        </AuthContext.Provider>
      </BrowserRouter>
    </div>
  );
}

export default App;
