import express from 'express';
import usersData from '../data/users-data.js';
import bing from '../services/bing.js';
import usersService from '../services/users-service.js';
import { authMiddleware } from '../auth/auth.middleware.js';
import bodyValidator from '../middlewares/body-validator.js';
import createUserScheme from '../validators/create-user-scheme.js';
import createPlayListScheme from '../validators/create-playList-scheme.js';
import updatePlayListScheme from '../validators/update-playList-scheme.js';
import tokenValidator from '../middlewares/token-validator.js';
import banGuard from '../middlewares/ban-guard.js';
import { upload } from '../middlewares/file-uploader.js';

const usersController = express.Router();

usersController
    .post('/', bodyValidator('users', createUserScheme), async (req, res) => {
      const body = req.body;

      const { error, user } = await usersService.createUser(usersData)(body.username, body.password);

      if (error) {
        res.status(409).send({ error: error });
      } else {
        res.status(201).send(user);
      }
    })
    .delete('/logout', authMiddleware, tokenValidator, async (req, res) => {
      const result = await usersData.logout(req.headers.authorization.replace('Bearer ', ''));

      if (result) {
        res.status(404).send(result);
      } else {
        res.send({ message: 'Successfully logged out' });
      }
    })
    .put('/avatar', authMiddleware, tokenValidator, banGuard, upload('avatars').single('avatar'), async (req, res) => {
      const userId = req.user.id;

      const avatarName = await usersData.updateAvatar(userId, req.file.filename);

      if (avatarName.error) {
        res.status(404).send(avatarName);
      } else {
        res.json({ message: `Avatar updated: ${avatarName}` });
      }
    })
    .get('/addresses', authMiddleware, tokenValidator, banGuard, async (req, res) => {
      const queries = req.query;
      const { error, result } = await bing(queries);

      if (error) {
        res.status(409).send({ error: error });
      } else {
        res.status(201).send({ duration: result.travelDuration, trafficDuration: result.travelDurationTraffic });
      }
    })

    .get('/playlists/getall', async (req, res) => {
      const { error, playLists } = await usersService.getPlaylists(usersData)(req.query);

      if (error) {
        res.status(400).send({ error });
      } else {
        res.status(200).send({ playLists });
      }
    })

    .get('/playlists/:duration', authMiddleware, tokenValidator, banGuard, async (req, res) => {
      const { duration } = req.params;
      const queries = req.query;

      const tracks = await usersService.playlist(usersData)(queries, duration);

      // const tracks = await usersService.playlistGenerator(usersData)(132, duration)

      if (tracks.error) {
        res.status(409).send(tracks);
      } else {
        res.send(tracks);
      }
    })

    .get('/playlists/get/:id', async (req, res) => {
      const { id } = req.params;

      const { error, playList } = await usersService.getPlayListById(usersData)(id);

      if (error) {
        res.status(400).send({ error });
      } else {
        res.status(200).send({ playList });
      }
    })

    .post('/playlists', authMiddleware, tokenValidator, banGuard, bodyValidator('playLists', createPlayListScheme), async (req, res) => {
      const userId = req.user.id;
      const playListData = req.body;

      const { error, playList } = await usersService.createPlayList(usersData)(userId, playListData);

      if (error) {
        res.status(400).send({ error });
      } else {
        res.status(200).send(playList);
      }
    })

    .get('/playlists/author/:id', async (req, res) => {
      const authorId = req.params.id;

      const playlists = await usersService.getPlaylistsByAuthor(usersData)(authorId);

      if (playlists.error) {
        res.status(400).send(playlists);
      } else {

        res.status(200).send(playlists);
      }
    })

    .delete('/playlists/:id', authMiddleware, tokenValidator, banGuard, async (req, res) => {
      const { id } = req.params;
      const user = req.user;

      const { error, playList } = await usersService.deletePlayList(usersData)(id, user);

      if (error) {
        res.status(400).send({ error });
      } else {
        res.status(200).send({ playList });
      }
    })

    .patch('/playlists/:id', authMiddleware, tokenValidator, banGuard, bodyValidator('playLists', updatePlayListScheme), async (req, res) => {
      const { id } = req.params;
      const playListData = req.body;
      const user = req.user;

      const { error, playList } = await usersService.updatePlayListById(usersData)(id, playListData, user);

      if (error) {
        res.status(400).send({ error });
      } else {
        res.status(200).send({ playList });
      }
    })

    .get('/tracks/:id', authMiddleware, async (req, res) => {
      const { id } = req.params;

      const { error, track } = await usersService.getTrackById(usersData)(id);

      if (error) {
        res.status(400).send({ error });
      } else {
        res.status(200).send({ track });
      }
    })

    .get('/artists/:id', async (req, res) => {
      const { id } = req.params;

      const artistInfo = await usersData.getArtistById(id);

      if (artistInfo.error) {
        res.status(400).send(artistInfo);
      } else if (artistInfo.length === 0) {
        res.status(400).send({ error: 'There is no artist with such id.' });
      } else {
        res.status(200).send(artistInfo[0]);
      }
    })

    .get('/genres/get', async (req, res) => {
      const { error, genres } = await usersService.getGenres(usersData)();

      if (error) {
        res.status(400).send({ error });
      } else {
        res.status(200).send({ genres });
      }
    })

    .get('/genres/:id', async (req, res) => {
      const { id } = req.params;

      const genresInfo = await usersData.getGenreById(id);

      if (genresInfo.error) {
        res.status(400).send(genresInfo);
      } else if (genresInfo.length === 0) {
        res.status(400).send({ error: 'There is no genre with such id.' });
      } else {
        res.status(200).send(genresInfo[0]);
      }
    })
    .get('/allgenres', async (req, res) => {

      const allGenresInfo = await usersData.getAllGenresInfo();

      if (allGenresInfo.error) {
        res.status(400).send(allGenresInfo);
      } else {
        res.status(200).send(allGenresInfo);
      }
    })
    .get('/:id', async (req, res) => {
      const { id } = req.params;

      const { error, user } = await usersService.getUser(usersData)(id);

      if (error) {
        res.status(400).send({ error });
      } else {
        res.send(user);
      }
    });

export default usersController;

/* Update playlist
{
    "playListTitle": "New Title",
        "genres": {
        "116":20,
        "132":20,
        "152":20,
        "464":20
        },
        "tags": ["From Plovdiv", "To Varna"]
}
*/
/*
{
    "playListTitle": "sfsfw",
    "tags": ["dddfdfd", "sddfsdf"],
    "genres": {
                "116":10,
                "132":20,
                "152":30,
                "464":40
                },
    "combinedGenresSongsDuration": 5000,
    "combinedGenresSongs": [
        {
            "track_id": 887340,
            "title": "Put The Boy Back In Cowboy",
            "link": "https://www.deezer.com/track/887340",
            "preview": "https://cdns-preview-e.dzcdn.net/stream/c-e7ed7c979cd4aa990bef2a504ec513d6-6.mp3",
            "duration": 239,
            "rank": 91424,
            "genres_genre_id": 152,
            "albums_album_id": "100637",
            "artists_artist_id": 637
        },
        {
            "track_id": 887341,
            "title": "I Love This Town",
            "link": "https://www.deezer.com/track/887341",
            "preview": "https://cdns-preview-a.dzcdn.net/stream/c-a24d3705b53edff9203d0f6ff9a971ec-6.mp3",
            "duration": 286,
            "rank": 113438,
            "genres_genre_id": 152,
            "albums_album_id": "100637",
            "artists_artist_id": 637
        }
    ]
}
*/
