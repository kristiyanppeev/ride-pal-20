import pool from './pool.js';
import { searchPlayList } from '../common/searchPlayList.js';

const create = async (username, password, role) => {
  const sql = `
    INSERT INTO users(username, password, roles_id)
    VALUES (?,?,(SELECT id FROM roles WHERE name = ?))
    `;
  let result = {};

  try {
    result = await pool.query(sql, [username, password, role]);
  } catch (error) {
    return { message: 'Something went wrong with create user request.' };
  }

  return {
    id: result.insertId,
    username: username,
  };
};

const getBy = async (column, value) => {
  const sql = `
    SELECT id, username
    FROM users
    WHERE ${column} = ?
    `;
  let result = [];
  try {
    result = await pool.query(sql, [value]);
  } catch (err) {
    return { error: 'Something went wrong with getBy request.' };
  }


  return result[0];
};

const getWithRole = async (username) => {
  const sql = `
    SELECT u.id, u.username, u.password, r.name as role
    FROM users u
    JOIN roles r ON u.roles_Id = r.id
    WHERE u.username = ? 
    `;
  let result = [];
  try {
    result = await pool.query(sql, [username]);
  } catch (err) {
    return { error: 'Something went wrong with getWithRole request.' };
  }

  return result[0];
};

const getUserInfo = async (id) => {
  const sql = `
    SELECT id, username, profile_pic
    FROM users
    WHERE id = ?
    `;
  let result = [];
  result = await pool.query(sql, [id]);
  try {
  } catch (err) {
    return { error: 'Something went wrong with get user info request.' };
  }

  return result;
};
const logout = async (token) => {
  const sql = `
    INSERT INTO deactivated_tokens (token) VALUES (?)
    `;
  try {
    await pool.query(sql, [token]);
  } catch (err) {
    return { error: 'Something went wrong with logout request.' };
  }
};

const isTokenDeactivated = async (token) => {
  const sql = `
    SELECT * FROM deactivated_tokens t WHERE t.token = ?
    `;
  let result = [];
  try {
    result = await pool.query(sql, [token]);
  } catch (error) {
    return { error: 'Something went wrong with token validating request.' };
  }


  return result[0] === undefined;
};

const updateAvatar = async (userId, file) => {
  const sql = `
    UPDATE users
    SET profile_pic = ? 
    WHERE (id = ?);
    `;
  try {
    await pool.query(sql, [file, userId]);
  } catch (error) {
    return { error: 'Something went wrong with update avatar request.' };
  }

  return file;
};

const getStatusById = async (userId) => {
  const sql = `
    SELECT is_banned, is_deleted
    FROM users
    WHERE id = ?
    `;

  try {
    const result = await pool.query(sql, [userId]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with get ban status request.' };
  }

};

const getTracksFromGenre = async (genreId) => {
  const sql = `
    SELECT *
    FROM tracks
    WHERE genres_genre_id = ?
    `;

  try {
    const result = await pool.query(sql, [genreId]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getTracksFromGenre sql request.' };
  }
};

const getGenreInfoByName = async (genreName) => {
  const sql = `
    SELECT *
    FROM genres
    WHERE name = ?
    `;

  try {
    const result = await pool.query(sql, [genreName]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getGenreInfoByName sql request.' };
  }
};

const getGenresNames = async () => {
  const sql = `
    SELECT name FROM genres
    `;

  try {
    const result = await pool.query(sql);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getGenresNames sql request.' };
  }
};


const insertTracksForPlayList = async (playListId, trackIds) => {
  // console.log(trackIds)
  const tracks = trackIds.reduce((acc, trackId) => {
    acc.push(`(${playListId}, ${trackId})`);
    return acc;
  }, [])
    .join(',');

  const sql = `
    INSERT INTO playlists_has_tracks (playlists_playlist_id, tracks_track_id)
    VALUES ${tracks};
    `;

  try {
    const result = await pool.query(sql);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with insertTracksForPlayList sql request.' };
  }
};

const insertGenresForPlayList = async (playListId, genres) => {
  // console.log(genres)
  const genreTypes = Object.keys(genres).reduce((acc, genre) => {
    acc.push(`(${playListId}, ${genre}, ${genres[genre]})`);
    return acc;
  }, [])
    .join(',');

  const sql = `
    INSERT INTO playlists_has_genres (playlists_playlist_id, genres_genre_id, genre_percentage)
    VALUES ${genreTypes};
    `;

  try {
    const result = await pool.query(sql);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with insertGenresForPlayList sql request.' };
  }
};


const insertTagsForPlayList = async (playListId, tags) => {
  // console.log(genres)
  const tagPairs = tags.map((tag) => `('${tag}', '${playListId}')`).join(',');
  const sql = `
    INSERT INTO playlists_tags (tag_name, playlists_playlist_id)
    VALUES ${tagPairs};
    `;

  try {
    const result = await pool.query(sql);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with insertTagsForPlayList sql request.' };
  }
};


const createPlayList = async (userId, playlist_title, duration, averageRank) => {
  const sql = `
    INSERT INTO playlists (playlist_title, duration, rank, users_id)
    VALUES (?, ?, ?, ?);
    `;

  try {
    const result = await pool.query(sql, [playlist_title, duration, averageRank, userId]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with createPlayList sql request.' };
  }
};

const deletePlayList = async (id) => {
  const sql = `
    UPDATE playlists
    SET is_deleted = ? 
    WHERE (playlist_id = ?);
    `;

  try {
    const result = await pool.query(sql, [1, id]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with deletePlayList sql request.' };
  }
};

const getPlayListById = async (id) => {
  const sql = `
    SELECT *
    FROM playlists
    WHERE (playlist_id = ? AND is_deleted = 0)
    `;

  try {
    const result = await pool.query(sql, [id]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getPlayListById sql request.' };
  }
};

const getPlayListGenresById = async (id) => {
  const sql = `
    SELECT *
    FROM playlists_has_genres
    WHERE (playlists_playlist_id = ?)
    `;

  try {
    const result = await pool.query(sql, [id]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getPlayListGenresById sql request.' };
  }
};

const getPlayListTracksById = async (id) => {
  const sql = `
    SELECT *
    FROM playlists_has_tracks
    WHERE (playlists_playlist_id = ?)
    `;

  try {
    const result = await pool.query(sql, [id]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getPlayListTracksById sql request.' };
  }
};

const getTrackById = async (id) => {
  const sql = `
    SELECT *
    FROM tracks
    WHERE (track_id = ?)
    `;

  try {
    const result = await pool.query(sql, [id]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getTrackById sql request.' };
  }
};


const getGenreIds = async () => {
  const sql = `
    SELECT genre_id
    FROM genres
    `;

  try {
    const result = await pool.query(sql);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getGenreIds sql request.' };
  }
};

const updatePlayListGenres = async (id, newGenreList) => {
  try {
    const result = await Promise.all(Object.keys(newGenreList).map(async (genre) => {
      const sql = `
            UPDATE playlists_has_genres
            SET genre_percentage = ${newGenreList[genre]} 
            WHERE (playlists_playlist_id = ${id} AND genres_genre_id = ${genre});
            `;

      await pool.query(sql);
    }));
    return result;
  } catch (error) {
    return { error: 'Something went wrong with updatePlayListGenres sql request.' };
  }
};

const updatePlayListTitles = async (id, newTitle) => {
  const sql = `
    UPDATE playlists
    SET playlist_title = ?
    WHERE (playlist_id = ?)
    `;

  try {
    const result = await pool.query(sql, [newTitle, id]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with updatePlayListTitles sql request.' };
  }
};

const updatePlayListTags = async (tagsToSet) => {
  try {
    const result = await Promise.all(tagsToSet.map(async (tag) => {
      const sql = `
              UPDATE playlists_tags
              SET tag_name = ?
              WHERE (id = ?)
              `;
      await pool.query(sql, [tag.tag_name, tag.id]);
    }));
    return result;
  } catch (error) {
    return { error: 'Something went wrong with updatePlayListTags sql request.' };
  }
};

const getPlayListTags = async (id) => {
  const sql = `
    SELECT id, tag_name FROM playlists_tags
    WHERE (playlists_playlist_id = ?)
    `;

  try {
    const result = await pool.query(sql, [id]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getPlayListTags sql request.' };
  }
};

const getPlaylistsIdsByQueries = async (title, duration, genres, page, pageSize) => {
  const durationRange = +searchPlayList.DURATIONRANGE;
  const durationNumber = +duration;
  const numberOfGenres = genres.split(';').length;

  const sql = `
    SELECT p.playlist_id
    FROM playlists as p
    ${genres.length > 0 ?
      `INNER JOIN playlists_has_genres as g ON g.playlists_playlist_id = p.playlist_id AND g.genre_percentage > 0 AND (
             ${genres.split(';').map((genre) => `g.genres_genre_id = ${+genre}`).join(' OR ')})` : ''}
    WHERE (p.is_deleted = 0 
        ${title.length > 0 ? `AND p.playlist_title LIKE "%${title}%"` : ''}
        ${duration.length > 0 ? ` AND p.duration <= ${(durationNumber + durationRange)} AND p.duration >= ${(durationNumber - durationRange)}` : ''}
        )
    GROUP BY p.playlist_id
    HAVING (COUNT(*) = ${numberOfGenres})
    ORDER BY p.rank DESC
    LIMIT ${page * pageSize}, ${pageSize}
    `;
  // http://localhost:5555/users/playlists/getall/?duration=1000&genres=464;116&page=0&pageSize=2
  // console.log(sql)
  try {
    const result = await pool.query(sql);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getPlaylistsIdsByQueries sql request.' };
  }
};

const getArtistById = async (artistId) => {
  const sql = `
  SELECT *
  FROM artists
  WHERE artist_id = ?
  `;

  try {
    const result = await pool.query(sql, [artistId]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with get artist by Id request.' };
  }
};

const getGenreById = async (genreId) => {
  const sql = `
  SELECT *
  FROM genres
  WHERE genre_id = ?
  `;

  try {
    const result = await pool.query(sql, [genreId]);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with get artist by Id request.' };
  }
};

const getGenres = async () => {
  const sql = `SELECT * FROM genres`

  try {
    const result = await pool.query(sql);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with getGenres sql request.' };
  }
};


const getAllGenresInfo = async () => {
  const sql = `
  SELECT * FROM genres
  `
  try {
    const result = await pool.query(sql);
    return result;
  } catch (error) {
    return { error: 'Something went wrong with get all genres request.' };
  }
};

const getPlaylistsByAuthorId = async (authorId) => {
    const sql = `
    SELECT *
    FROM playlists
    WHERE users_id = ?
    `;

    try {
      const result = await pool.query(sql, [authorId]);
      return result;
    } catch (error) {
      return { error: 'Something went wrong with get all genres request.' };
    }
};

export default {
  getTracksFromGenre,
  getGenreInfoByName,
  getGenresNames,
  create,
  getWithRole,
  getBy,
  getUserInfo,
  logout,
  isTokenDeactivated,
  updateAvatar,
  getStatusById,
  createPlayList,
  insertGenresForPlayList,
  insertTracksForPlayList,
  deletePlayList,
  getPlayListById,
  getPlayListGenresById,
  getPlayListTracksById,
  getTrackById,
  getGenreIds,
  updatePlayListGenres,
  updatePlayListTitles,
  insertTagsForPlayList,
  updatePlayListTags,
  getPlayListTags,
  getPlaylistsIdsByQueries,
  getArtistById,
  getGenreById,
  getGenres,
  getAllGenresInfo,
  getPlaylistsByAuthorId
};


// const sql = `
// SELECT *
// FROM playlists as p
// ${genres.length > 0 ?
//     `INNER JOIN playlists_has_genres as g ON g.playlists_playlist_id = p.playlist_id AND g.genre_percentage > 0 AND (
//          ${genres.split(";").map(genre => `g.genres_genre_id = ${+genre}`).join(" OR ")})` : ""}
// WHERE (p.is_deleted = 0
//     ${name.length > 0 ? `AND p.playlist_title LIKE "%${name}%"` : ""}
//     ${duration.length > 0 ? ` AND p.duration <= ${(durationNumber + durationRange)} AND p.duration >= ${(durationNumber - durationRange)}` : ""}
//     )
// GROUP BY p.playlist_id
// ORDER BY p.rank DESC
// LIMIT ${page * pageSize}, ${pageSize}
// `;
