import bcrypt from 'bcrypt';
import { dbTableVolumes } from '../common/dbTableVolumes.js';
import errorStrings from '../common/error-strings.js';
import { userRole } from '../common/user-role.js';
import { searchPlayList } from '../common/searchPlayList.js';


const createUser = (usersData) => {
  return async (username, password) => {
    const existingUser = await usersData.getBy('username', username);

    if (existingUser) {
      if (existingUser.error) {
        return {
          error: existingUser.error,
          user: null,
        };
      }
      return {
        error: errorStrings.users.userAlreadyExists,
        user: null,
      };
    }

    const passwordHash = await bcrypt.hash(password, 10);
    const user = await usersData.create(username, passwordHash, userRole.user);

    if (user.message) {
      return {
        error: user.message,
        user: null,
      };
    }

    return {
      error: null,
      user: user,
    };
  };
};

const signInUser = (usersData) => {
  return async (username, password) => {
    const user = await usersData.getWithRole(username);

    if (user && user.error) {
      return {
        error: user.error,
        user: null,
      };
    }

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return {
        error: errorStrings.users.invalidCredentials,
        user: null,
      };
    }
    return {
      error: null,
      user: user,
    };
  };
};

const getUser = (usersData) => {
  return async (id) => {
    const user = await usersData.getUserInfo(id);

    if (user && user.error) {
      return {
        error: user.error,
        user: null,
      };
    }

    if (user.length === 0) {
      return {
        error: errorStrings.users.invalidUserId,
        user: null,
      };
    }

    if (user[0].profile_pic === null) {
      user[0].profile_pic = 'default.png';
    }

    return {
      error: null,
      user: user[0],
    };
  };
};


const playlistGenerator = (usersData) => {
  return async (genreId, travelDuration) => {
    const allTracks = await usersData.getTracksFromGenre(genreId);

    if (allTracks.error) {
      return allTracks;
    }

    const allTracksLength = allTracks.length;

    const resultingPlaylist = [];

    const subArrayLength = 15;

    const targetSeconds = travelDuration;

    const subArrayStartingPoint = Math.floor(Math.random() * ((allTracksLength - subArrayLength) - 1));

    const subArray = allTracks.slice(subArrayStartingPoint, subArrayStartingPoint + subArrayLength);

    allTracks.splice(subArrayStartingPoint, subArrayLength);

    const current = {
      sum: 0,
      elements: [],
    };

    while (current.sum < targetSeconds - (160 * subArrayLength)) {
      const randomIndex = Math.floor(Math.random() * allTracks.length);

      current.sum += allTracks[randomIndex].duration;
      current.elements.push(allTracks[randomIndex]);

      allTracks.splice(randomIndex, 1);
    }

    const subsetSums = (arr, sum, arrLength, depth = 0, elements = [], previousSum = 0) => {
      if (previousSum !== sum) {
        elements = [...elements, subArray[depth - 1]];
      }

      if (depth > arrLength) {
        resultingPlaylist.push({ sum, elements });

        return;
      }

      subsetSums(arr, sum + arr[depth], arrLength, depth + 1, elements, sum);

      subsetSums(arr, sum, arrLength, depth + 1, elements, sum);
    };

    const subArrayDurations = subArray.map((el) => el.duration);

    subsetSums(subArrayDurations, 0, subArrayLength - 1);

    const difference = resultingPlaylist.map((el) => Math.abs((targetSeconds - current.sum) - el.sum));

    const smallestNumber = difference.reduce((acc, el, index) => {
      if (index === 0) {
        acc = el;
        return acc;
      }

      if (acc > el) {
        return el;
      }
      return acc;
    }, 0);

    const bufferArr = resultingPlaylist[difference.indexOf(smallestNumber)];

    const sumOfAllSongsDuration = current.sum + bufferArr.sum;

    const songsToBePlayed = [...current.elements, ...bufferArr.elements];

    return {
      sumOfAllSongsDuration,
      songsToBePlayed,
    };
  };
};


const playlist = (usersData) => {
  return async (queries, duration) => {
    const allGenres = await usersData.getGenresNames();

    if (allGenres.error) {
      return allGenres;
    }

    const allGenresStrings = allGenres.map((element) => {
      if (element.name === 'Rap/Hip Hop') {
        element.name = 'Rap';
      }
      element.name = element.name.toLowerCase();

      return element.name;
    });

    const errorObj = { error: null };

    Object.keys(queries).forEach((element) => {
      const elementLowerCase = element.toLowerCase();

      if (!allGenresStrings.includes(elementLowerCase)) {
        errorObj.error = `Possible genres to search for are ${allGenresStrings}`;
      }
    });

    const sumOfGenresPercents = Object.values(queries).reduce((acc, el) => {
      acc += +el;
      return acc;
    }, 0);

    if (sumOfGenresPercents !== 100) {
      errorObj.error = `The sum of genres percents must be 100.`;
    }

    const sortedQueries = Object.fromEntries(
      Object.entries(queries).sort(([, a], [, b]) => a - b)
    );

    if (errorObj.error) {
      return errorObj;
    }

    let margin = 0;

    const sortedGenres = Object.keys(sortedQueries);

    const genrePlaylists = [];

    for (let i = 0; i < sortedGenres.length; i++) {
      const genreDuration = Math.floor((sortedQueries[sortedGenres[i]] / 100) * duration);

      if (sortedGenres[i].toLocaleLowerCase() === 'rap') {
        sortedGenres[i] = 'Rap/Hip Hop';
      }

      const genreInfo = await usersData.getGenreInfoByName(sortedGenres[i]);

      if (genreInfo.error) {
        errorObj.error = genreInfo.error;
      }

      const targetSeconds = genreDuration + margin;

      const generatedPlaylist = await playlistGenerator(usersData)(genreInfo[0].genre_id, targetSeconds);

      margin = genreDuration - generatedPlaylist.sumOfAllSongsDuration + margin;

      genrePlaylists.push(generatedPlaylist);
    }

    if (errorObj.error) {
      return errorObj;
    }

    const combinedGenresSongs = genrePlaylists.reduce((acc, el) => {
      acc = [...acc, ...el.songsToBePlayed];

      return acc;
    }, []).sort(() => Math.random() - 0.5);

    const combinedGenresSongsDuration = genrePlaylists.reduce((acc, el) => {
      acc += el.sumOfAllSongsDuration;
      return acc;
    }, 0);

    return { combinedGenresSongsDuration, combinedGenresSongs };
  };
};

const createPlayList = (usersData) => {
  return async (userId, playListData) => {

    // Get validated Title, Duration, Create Average Rank
    const playlist_title = playListData.playListTitle;
    const duration = playListData.combinedGenresSongsDuration;
    const averageRank = playListData.combinedGenresSongs.reduce((averageRank, { rank }) => {
      averageRank = +averageRank + +rank;
      return averageRank;
    }, 0) / playListData.combinedGenresSongs.length;

    const roundedAvgRank = averageRank.toFixed(0);

    // Validate genres
    const genres = playListData.genres;

    const genreIds = await usersData.getGenreIds();

    if (genreIds && genreIds.error) return { error: genreIds.error };

    // Validate if genres exist in prepared list
    const genreIdsArr = await genreIds.map(genreObj => genreObj.genre_id);
    const badGenres = Object.keys(genres).filter(genre => !genreIdsArr.includes(+genre));

    if (badGenres.length > 0) return { error: "Generes must be from the included options." };

    // If not enough genres are provided, fill the rest from the existing with 0 percentage
    const availableGenres = await genreIds.map((genre) => genre.genre_id);
    if (Object.keys(genres).length !== availableGenres.length) {
      availableGenres.map((genre) => {
        if (!genres[genre]) {
          genres[genre] = 0;
        }
      });
    }


    // Validate tracks
    const trackIds = playListData.combinedGenresSongs.map((track) => track.track_id);

    const validateTracks = await Promise.all(
      playListData.combinedGenresSongs.map(async (track) => {
        const realTrackArr = await usersData.getTrackById(track.track_id);

        if (realTrackArr && realTrackArr.error) {
          return { realTrackArr };
        }
        const validateTrackObj = await realTrackArr[0];
        if (JSON.stringify(track) === JSON.stringify(validateTrackObj)) {
          return true;
        }
        return false;
      }))

    if (validateTracks.includes(false)) {
      return { error: "One or more of the tracks does not exist in DB." };
    }
    const checkForErrorObj = validateTracks.filter(res => typeof res === 'object');

    if (checkForErrorObj.length > 0) {
      return { error: checkForErrorObj[0].realTrackArr.error };
    }

    // Validate tags
    const tags = playListData.tags;
    if (tags.length > dbTableVolumes.TAGS) return { error: `Tags can't be more than ${dbTableVolumes.tags}` };
    if (tags.length < dbTableVolumes.TAGS) {
      const pushes = dbTableVolumes.TAGS - tags.length;
      for (let i = 0; i < pushes; i++) {
        tags.push(null);
      }
    }


    // Create the playlist in DB
    const playList = await usersData.createPlayList(userId, playlist_title, duration, roundedAvgRank);
    if (playList && playList.error) return { playList };

    const playListId = await playList.insertId;

    // Create the genres for the playlist in DB
    const insertGenres = await usersData.insertGenresForPlayList(playListId, genres);
    if (insertGenres && insertGenres.error) return { error: insertGenres.error };

    // Create the tracks for the playlist in DB
    const insertTracks = await usersData.insertTracksForPlayList(playListId, trackIds);
    if (insertTracks && insertTracks.error) return { error: insertTracks.error };

    // Create the tags for the playlist in DB
    const insertTags = await usersData.insertTagsForPlayList(playListId, tags);
    if (insertTags && insertTags.error) return { error: insertTags.error };

    // Get all data for the created playlist and return it
    const playListWithTagsTracksGenres = await getPlayListById(usersData)(playListId);
    if (playListWithTagsTracksGenres.error !== null) return { error: playListWithTagsTracksGenres.playList.error };

    if (playList && playList.error) {
      return {
        error: playList.error,
        playList: null,
      };
    }

    return {
      error: null,
      playList: playListWithTagsTracksGenres.playList[0],
    };
  };
};

const deletePlayList = (usersData) => {
  return async (id, user) => {
    const checkPlaylist = await usersData.getPlayListById(id);
    if (checkPlaylist && checkPlaylist.error) return { error: checkPlaylist.error };
    if (checkPlaylist.length === 0) return { error: 'There is no such playlist.' };
    if ((checkPlaylist[0].users_id !== user.id) && (user.role !== userRole.admin)) return { error: 'You are not the owner of the playlist.' };

    const playListToSend = await getPlayListById(usersData)(id);
    if (playListToSend.error !== null) return { error: playListToSend.error };
    const playListToSendClean = await playListToSend.playList[0];

    const playList = await usersData.deletePlayList(id);

    if (playList && playList.error) {
      return {
        error: playList.error,
        playList: null,
      };
    }

    return {
      error: null,
      playList: playListToSendClean,
    };
  };
};

const getPlayListById = (usersData) => {
  return async (id) => {
    const playList = await usersData.getPlayListById(id);
    if (playList && playList.error) return { error: playList.error };

    if (playList.length === 0) return { error: 'There is no such playlist.' };

    const genres = await usersData.getPlayListGenresById(id);
    if (genres && genres.error) return { error: genres.error };

    playList[0].genres = await genres.reduce((acc, genre) => {
      acc[genre.genres_genre_id] = genre.genre_percentage;
      return acc;
    }, {});

    const tags = await usersData.getPlayListTags(id);
    if (tags && tags.error) return { error: tags.error };

    playList[0].tags = await tags.reduce((acc, tag) => {
      if (tag.tag_name !== null && tag.tag_name !== 'null') acc.push(tag.tag_name);
      return acc;
    }, []);

    const tracks = await usersData.getPlayListTracksById(id);
    if (tracks && tracks.error) return { error: tracks.error };

    const tracksWithInfo = await Promise.all(tracks.map(async (track) => {
      return await usersData.getTrackById(track.tracks_track_id);
    }));

    const checkForErrorObj = tracksWithInfo.filter(res => res.hasOwnProperty('error'));

    if (checkForErrorObj.length > 0) return { error: checkForErrorObj[0].error };

    playList[0].combinedGenresSongs = tracksWithInfo.reduce((acc, track) => {
      acc.push(...track);
      return acc;
    }, []);

    if (playList && playList.error) {
      return {
        error: playList.error,
        playList: null,
      };
    }

    return {
      error: null,
      playList: playList,
    };
  };
};

const updatePlayListById = (usersData) => {
  return async (id, playListData, user) => {
    const newTitle = playListData.playListTitle;
    const newTags = playListData.tags;

    const playList = await usersData.getPlayListById(id);
    if (playList && playList.error) return { error: playList.error };
    if (playList.length === 0) return { error: 'There is no such playlist.' };
    if ((playList[0].users_id !== user.id) && (user.role !== userRole.admin)) return { error: 'You are not the owner of the playlist.' };

    const updateTitle = await usersData.updatePlayListTitles(id, newTitle);
    if (updateTitle && updateTitle.error) return { error: updateTitle.error };

    const genreIds = await usersData.getGenreIds();
    if (genreIds && genreIds.error) return { error: genreIds.error };

    if (newTags.length > dbTableVolumes.TAGS) return { error: `Tags can't be more than ${dbTableVolumes.TAGS}` };

    const currentTags = await usersData.getPlayListTags(id);
    if (currentTags && currentTags.error) return { error: currentTags.error };

    const tagsToSet = await currentTags.map((tag) => {
      return { id: tag.id, tag_name: null };
    })
      .map((tag) => {
        if (newTags.length !== 0) {
          tag.tag_name = newTags.shift();
          return tag;
        } return tag;
      });

    const updateTags = await usersData.updatePlayListTags(tagsToSet);
    if (updateTags && updateTags.error) return { error: updateTags.error };

    if (playList && playList.error) {
      return {
        error: playList.error,
        playList: null,
      };
    }

    return {
      error: null,
      playList: playList,
    };
  };
};

const getTrackById = (usersData) => {
  return async (id) => {
    const track = await usersData.getTrackById(id);

    if (track && track.error) {
      return {
        error: track.error,
        track: null,
      };
    }

    return {
      error: null,
      track: track,
    };
  };
};

const getPlaylists = (usersData) => {
  return async (queries) => {
    let playLists = [];

    const title = queries.title || '';
    const duration = queries.duration || '';
    const genres = queries.genres || '';

    const page = queries.page || searchPlayList.PAGE;
    const pageSize = queries.pageSize || searchPlayList.PAGESIZE;

    // Validate title
    if (title !== '' && title.length < 3 && title.length > 50) {
      return { error: "The title must be a text between 3 and 50 symbols." }
    }

    // Validate duration
    if (duration !== '') {
      if ( isNaN(duration) || +duration < 1) {
        return { error: "Duration must be a positive number." }
      }
    }

    // Validate genres
    const allAboutGenres = await usersData.getGenres();
    if (allAboutGenres && allAboutGenres.error) return { error: allAboutGenres.error };
    const allGenreIds = await allAboutGenres.map(genre => genre.genre_id)
    const genreErrors = genres.split(';').filter(genre => !allGenreIds.includes(+genre))
    if (genres !== '' && genreErrors.length > 0) {
      return { error: "Genres must be from the specified genres." }
    }
    // Validate page
    if (isNaN(page) || page < 0) return { error: "Page must be a positive number." }
    // Validate pageSize
    if (isNaN(pageSize) || pageSize < 1) return { error: "pageSize must be a positive number." }

    const playListsIds = await usersData.getPlaylistsIdsByQueries(title, duration, genres, page, pageSize);
    if (playListsIds && playListsIds.error) return { error: playListsIds.error };

    const playListsAddTagsGenresTracks = await Promise.all(await playListsIds
      .map((list) => list.playlist_id)
      .map(async (listId) => await getPlayListById(usersData)(listId)));

    const checkForErrorObj = playListsAddTagsGenresTracks.filter(res => res.error !== null);

    if (checkForErrorObj.length > 0) return { error: checkForErrorObj[0].playList.error };

    playLists = playListsAddTagsGenresTracks.map((list) => list.playList[0]);

    if (playLists && playLists.error) {
      return {
        error: playLists.error,
        playLists: null,
      };
    }

    return {
      error: null,
      playLists: playLists,
    };
  };
};

const getGenres = (usersData) => {
  return async () => {
    const genres = await usersData.getGenres();

    if (genres && genres.error) {
      return {
        error: genres.error,
        genres: null,
      };
    }

    return {
      error: null,
      genres: genres,
    };
  };
};

const getPlaylistsByAuthor = usersData => {
  return async (authorId) => {

    const userPlaylilstUnfiltered = await usersData.getPlaylistsByAuthorId(authorId);

    if(userPlaylilstUnfiltered.error) {
      return userPlaylilstUnfiltered;
    };

    const nonDeletedPlaylists = userPlaylilstUnfiltered.filter(el => el.is_deleted === 0);

    return nonDeletedPlaylists;

    // const playlistsGenres = nonDeletedPlaylists.map( async (el) => {
      
    //   const result = await usersData.getPlayListGenresById(el.playlist_id);
    //   const resultMapped = result.map(el => {
    //     return { [el.genres_genre_id]:  el.genre_percentage}
    //   })

    //   return {
    //     ...el,
    //     genres: resultMapped
    //   }
    // });

    // await Promise.all(playlistsGenres).then(playlistsWithGenres => {
    //   const playlistsWithTracks = playlistsWithGenres.map(async (el) => {
    //     const result = await usersData.getPlayListTracksById(el.playlist_id);
    //     console.log(result)
    //   })
      
    // })
  }
}

export default {
  playlistGenerator,
  playlist,
  signInUser,
  createUser,
  getUser,
  createPlayList,
  deletePlayList,
  getPlayListById,
  updatePlayListById,
  getTrackById,
  getPlaylists,
  getGenres,
  getPlaylistsByAuthor
};
